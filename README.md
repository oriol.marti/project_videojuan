
# VideoJuan

- El nostre client segueix sen el senyor Juan, la tenda que vem fer al primer projecte va acabar malament, per el que el pobre senyor s’ha arruïnat, ara a venut la seva tenda i s’ha retirat, per el que vol gaudir dels seus hobbies. Com es un aficionat als videojocs vol que fem una revista de videojocs que pugui monetizar.

<img src="https://gitlab.com/oriol.marti/project_videojuan/-/raw/master/logo_videojuan.png" alt="drawing" width="300"/>

# Command line instructions
### You can also upload existing files from your computer using the instructions below.


- Git global setup
- git config --global user.name "example"
- git config --global user.email "example@email"

- Push an existing folder
- cd existing_folder
- git init --initial-branch=main
- git remote add origin https://gitlab.com/oriol.marti/project_videojuan.git
- git add .
- git commit -m "Initial commit"
- git push -u origin main

- Push an existing Git repository
- cd existing_repo
- git remote rename origin old-origin
- git remote add origin https://gitlab.com/oriol.marti/project_videojuan.git
- git push -u origin --all
- git push -u origin --tags
