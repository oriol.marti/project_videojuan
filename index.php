<!DOCTYPE html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (!isset($_SESSION)) {
	session_start();
};
?>
<html>

<?php
require './header.php';
$myVideogame = new Videogame($conn);
?>

<head>
	<link rel="stylesheet" href="/indexStyles.css">
</head>
<body>
	<div class="container-fluid ">
		<h1 class="title_h1">TOP GAMES</h1>
		<section>
			<div class="container2">
				<div class="row" id="row">
					<?php
					$cont = 0;
					$a_myVideogame = [];
					if ($a_myVideogame = $myVideogame->llista()) {
						foreach ($a_myVideogame as $myVideogameTMP) {
							$cont++;
					?>
							<div class="col-sm-3" name="carta" id="carta_<?= $cont ?>">
								<div class="card">
									<div class="card__image-container">
										<img class="card__image" src="<?= $myVideogameTMP['videogameLogo'] ?>" alt="">
									</div>

									<svg class="card__svg" viewBox="0 0 800 500">

										<path d="M 0 100 Q 50 200 100 250 Q 250 400 350 300 C 400 250 550 150 650 300 Q 750 450 800 400 L 800 500 L 0 500" stroke="transparent" fill="#333" />
										<path class="card__line" d="M 0 100 Q 50 200 100 250 Q 250 400 350 300 C 400 250 550 150 650 300 Q 750 450 800 400" stroke="pink" stroke-width="3" fill="transparent" />
									</svg>
									<a href="./game_details.php?value=<?= $myVideogameTMP['idVideogame'] ?>">
											<h3 class="card__title">
												<?= $myVideogameTMP['videogameName'] ?>
											</h3>
										</a>
									<div class="card__content">
										
										<p><?= $myVideogameTMP['videogameSynopsis'] ?></p>
									</div>
								</div>
							</div>
					<?php
							if ($cont == 8) {
								break;
							}
						}
					}
					?>
					<script>
						if ($('#carta_7').is(':hidden')) {
							document.getElementsByName("carta").className = "col-sm-4";
						}
					</script>
				</div>
			</div>
		</section>
	</div>
	<?php
	require './footer.html';
	require './scripts.html';
	?>
</body>

</html>