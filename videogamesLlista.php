<?php 
//0) activo els errors
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

//1) Activo la sessió
session_start();


if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require './header.php';
$myVideogame = new Videogame($conn);
if(!isset($_REQUEST['filtre'])) $filtre = '';
else $filtre = $_REQUEST['filtre'];
$sql = 'videogameName LIKE  \'%' . $filtre . '%\'';
$total = $myVideogame->numTotal($sql);
$results_per_page = 10;
$numPag = ceil($total/$results_per_page);
if (!isset ($_GET['page']) ) {  
	$pageActual = 1;  
} else {  
	$pageActual = $_GET['page'];  
}
$page_first_result = ($pageActual-1) * $results_per_page;
$limit = $page_first_result.','.$results_per_page;


?>

<head>
	<link rel="stylesheet" href="/studiesLlista.css">
</head>

<body>
	<h1 class="text-center mt-2">VIDEOJOCS</h1>
	<div class="container-fluid llista">
		<section>
			<form action="" class="search" autocomplete="off">
				<input type="text" placeholder="Buscar per nom..." name="filtre" onblur="submit()" value="<?=$filtre?>">
		<table class="table table-hover table-striped">
			<tbody>
		<?php	
			
		$a_myVideogame = [];
		if($a_myVideogame = $myVideogame->llista($sql,$limit)){
			foreach($a_myVideogame as $myVideogameTMP){
		?>
				<tr>
					<td class="col-2"><a href="/game_details.php?value=<?=$myVideogameTMP['idVideogame']?>"><img class="list-img" src="<?=$myVideogameTMP['videogameLogo']?>" alt=""></a></td>
					<td class="col-6"><a class="h5 text-white" href="/game_details.php?value=<?=$myVideogameTMP['idVideogame']?>"><?=$myVideogameTMP['videogameName']?></a><br><p class="pText"><?=$myVideogameTMP['videogameSynopsis']?></p></td>
					<td class="col-2"><p class="pLlista">Likes: <?=$myVideogameTMP['videogameLikes']?></p><p class="pLlista mb-0">Dislikes: <?=$myVideogameTMP['videogameDislikes']?></p><br>
					<p class="pStar">
					<?php 
					$cont = 5;
					for($i =0;$i<$myVideogameTMP['videogameRating'];$i++){
						echo "★";
						$cont--;
					}
					for($i=0;$i<$cont;$i++){
						echo "☆";
					} 
					?>
					</p></td>
				</tr>
			<?php 
			}
		}
			?>
			</tbody>
		</table>
		<nav class="paginacio">
			<ul class="pagination justify-content-center">
				<li class="page-item <?php if($pageActual==1)echo 'disabled';?>">
					<a class="page-link" href="<?=$_SERVER['PHP_SELF']. '?page=' . ($pageActual-1).'&filtre='.$filtre ?>">Previous</a>
				</li>
				<?php
				for($page = 1; $page<= $numPag; $page++) {  
				?>
				<li class="page-item nums <?php if($pageActual == $page)echo"active";?>"><a onclick="submit()" class="page-link" href = " <?=$_SERVER['PHP_SELF']. '?page=' . $page.'&filtre='.$filtre ?> "> <?=$page?> </a></li>
				<?php
				}
				?>
				<li class="page-item <?php if($numPag<=$pageActual)echo 'disabled'; ?>">
					<a class="page-link" href="<?=$_SERVER['PHP_SELF']. '?page=' . ($pageActual+1).'&filtre='.$filtre ?>">Next</a>
				</li>
			</ul>
			<p class="text-center paginaActual">Pagina: <?=$pageActual?> de <?=$numPag?></p>
			
		</nav>
		</form>
	</section>
	</div>
    <?php
	require './footer.html';
	require './scripts.html';
	?>
</body>

</html>