<?php 
//0) activo els errors
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();


if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require './header.php';
?>
<body>
	<h1 class="text-center mt-2">Flappy Bird</h1>
	<div class="container-fluid llista">
		<section>
            <table class="table table-hover table-striped">
                    <tr>
                        <td>
                            <div class="ranking d-flex">
                                <div class="col-md-8 col-sm-12" style="height: 660px;"><iframe src="/jocs/JOC2/flappyBird.php" frameborder="0" style="height: 100%; width: 100%;"></iframe></div>
                                <div class="ranking">
                                    <div class="col-2"><p class="pLlista">Ranking:</p></div>
                                    <div class="col-2"><p class="pLlista">
                                        <?php
                                        $myUser = new userService($conn);
                                        $a_users = $myUser->llistar('puntuacioJoc2>0','puntuacioJoc2 DESC');
                                        $cont = 0;
                                        foreach($a_users as $users){
                                            echo $users['username'] . '-> ';
                                            echo $users['puntuacioJoc2'];
                                            echo '<br>';
                                            $cont++;
                                            if($cont==5)break;
                                        }
                                        ?>
                                    </p><br></div>
                                </div>
                            </div>
                        </td>
                    </tr>
            </table>
            
	    </section>
	</div>
    <?php
	require './footer.html';
	require './scripts.html';
	?>
</body>

</html>