<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

$myRetailer = new Retailer($conn);
?>

<body>
<h1 class="text-center mt-2">DISTRIBUIDORS</h1>
<div class="container-fluid p-5">
	<section>	
	<a href="addRetailer.php" class='btn btn-secondary m-2'>Crea un distribuidor nou</a>
	</section>
	<section>
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Nom</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$a_myRetailer = [];
			if($a_myRetailer = $myRetailer->llista()){
				foreach($a_myRetailer as $myRetailerTMP){
				?>
				<tr>
					<th scope="row"><?=$myRetailerTMP['idRetailer']?></th>
					<td class="col-8"><?=$myRetailerTMP['retailerName']?></td>
					<td>
						<form method='POST' action='modifyRetailer.php'>
							<input type='hidden' name='idRetailer' value="<?=$myRetailerTMP['idRetailer']?>" >
							<button class='btn btn-secondary' type='submit'> Edit </button>
						</form>
					</td>
					<td>
						<form method='POST' action='retailersOperacions.php'>
							<input type='hidden' name='deleterid' value="<?=$myRetailerTMP['idRetailer']?>">
							<input type="hidden" name="operacio" value="elimina">
							<button class='btn btn-danger' type='submit'>Delete</button>
						</form>
					</td>
				</tr>
				<?php
			}}
			?>
			</tbody>
		</table>
	</section>
</div>
</body>

</html>