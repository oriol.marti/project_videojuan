<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

$myRetailer = new Retailer($conn);
?>
<html>
    <body>
    <?php
    $a_myRetailer = [];
    if($a_myRetailer = $myRetailer->llista()){
        foreach($a_myRetailer as $myRetailerTMP){
            if($myRetailerTMP['idRetailer'] == $_REQUEST['idRetailer']){
    ?>
    <section>
        <div class="container p-5">
            <form action="retailersOperacions.php">
                <input type="hidden" name="idRetailer" value="<?= $myRetailerTMP['idRetailer'] ?>">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="nom" class="textmuted h8">Nom</label>
                    <input type="text" class="form-control" name="retailerName" id="retailerName" value="<?= $myRetailerTMP['retailerName'] ?>" required>
                </div>
            </div>
            <input type="hidden" name="operacio" value="modificar">
            <button type="submit" class="btn btn-danger">Submit</button>
            </form>
        </div>
    </section>
    <?php }}}?>
</body>
</html>