<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');;
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
}

$operacio = $_REQUEST['operacio'];
$id = $_REQUEST['idRetailer'];
$retailerName = $_REQUEST['retailerName'];
$myRetailer = new Retailer($conn);

switch($operacio){
    case 'elimina':
        $myRetailer->deleteRetailer($_REQUEST['deleterid']);
        break;
    case 'modificar':
        $myRetailer->updateRetailer($retailerName,$id);
        break;
    case 'insertar':
        $myRetailer->insertRetailer($retailerName);
        break;
}header("Location: retailers.php");

?>
