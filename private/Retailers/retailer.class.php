<?php

class Retailer{
    
    public $conn;
    private $retailerList = [];

    function __construct($lConn){
        $this->conn = $lConn;
    }

    function llista($filtre=0){
        if(!$filtre)$filtre='';
        else $filtre = ' WHERE ' . $filtre;
        $laMevaSentencia = $this->conn->prepare("SELECT * FROM retailers $filtre");
		$laMevaSentencia->execute();
        $this->retailerList=$laMevaSentencia->fetchAll();
        return($this->retailerList);
    } 

    function insertRetailer($retailerName){
        $insertador = $this->conn->prepare("INSERT INTO retailers (retailerName) VALUES ('".$retailerName."');");
        $insertador->execute();
    }

    function deleteRetailer($id){
        $deleteador = $this->conn->prepare("DELETE FROM retailers WHERE idRetailer=".$id."");
        $deleteador->execute();
    }

    function updateRetailer($retailerName,$id){
        $modificador = $this->conn->prepare("UPDATE retailers SET retailerName = '$retailerName' WHERE idRetailer = $id");
        $modificador->execute();
    }
    
}