<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

/* if(!isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
} */ 

$usuario = new userService($conn);
if(!empty($_SESSION['user'])){
 $suser = unserialize($_SESSION['user']);
 $usuario->setuser($suser);  
}

$idUser = $usuario->getId();

$operacio = $_REQUEST['operacio'];
$idComent = $_REQUEST['idComent'];
if(!isset($_REQUEST['idVideogame'])) $idVideogame = 0;
else $idVideogame = $_REQUEST['idVideogame'];
if(!isset($_REQUEST['idStudy'])) $idStudy = 0;
else $idStudy = $_REQUEST['idStudy'];
$idStudy = $_REQUEST['idStudy'];
$public = $_REQUEST['public'];
$private = $_REQUEST['private'];
$publicStudy = $_REQUEST['publicStudy'];
$comentTxt = addslashes($_REQUEST['comentTxt']);
$myComent = new Coment($conn);

switch($operacio){
    case 'elimina':
        if($myComent->set('idComent',$idComent)){
            $myComent->deleteComent();
            if($public == 'public'){
                header("Location: ../../game_details.php?value=".$idVideogame);
            }else if($private == 'private') {
                header("Location: coments.php");
            }else{
                header("Location: ../../study_details.php?value=".$idStudy);
            }
        }
        break;
    case 'modificar':
        if($myComent->set('idComent', $idComent) && $myComent->set('idVideogame', $idVideogame)){
            $myComent->updateComent();
        }
        break;
    case 'insertar':
        if($myComent->set('comentTxt',$comentTxt) && $myComent->set('idUser',$idUser)){
            if($idVideogame != 0){
                $myComent->set('idVideogame',$idVideogame);
                if($myComent->insertComent()){
                    header("Location: ../../game_details.php?value=".$idVideogame);
                }else{
                    die;
                };
            }elseif($myComent->set('idStudy',$idStudy)){
                if($myComent->insertComent()){
                    header("Location: ../../study_details.php?value=".$idStudy);
                }else{
                    die;
                };
            }
        }
    case 'Like':
        if($myComent->set('idComent', $idComent) && $myComent->set('idUser',$idUser))
            $myComent->insertLikes('idComent= ' .$idComent);
            $myComent->insertRel();
            $lastId = $myComent->get('idComent');
            if($public == 'public'){
                header("Location: ../../game_details.php?value=".$idVideogame);
            }else {
                header("Location: ../../study_details.php?value=".$idStudy);
            }
        break;
    case 'Dislike':
        if($myComent->set('idComent', $idComent) && $myComent->set('idUser',$idUser))
            $myComent->insertDislikes('idComent= ' .$idComent);
            $myComent->insertRel();
            $lastId = $myComent->get('idComent');
            if($public == 'public'){
                header("Location: ../../game_details.php?value=".$idVideogame);
            }else {
                header("Location: ../../study_details.php?value=".$idStudy);
            }
        break;
}//header("Location: ../../game_details.php?value=".$idVideogame);

?>
