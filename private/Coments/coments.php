<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

$myComent = new Coment($conn);
?>

<body>
<h1 class="text-center mt-2">COMENTS</h1>
<div class="container-fluid p-5">
	<section>
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Nom</th>
                    <th>Esborra</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$a_myComent = [];
			if($a_myComent = $myComent->llista()){
				foreach($a_myComent as $myComentTMP){
				?>
				<tr>
					<th scope="row"><?=$myComentTMP['idComent']?></th>
					<td class="col-8"><?=$myComentTMP['comentTxt']?></td>
					<td>
						<form method='POST' action='comentsOperacions.php'>
							<input type='hidden' name='idComent' value="<?=$myComentTMP['idComent']?>">
                            <input type="hidden" name="private" value="private">
							<input type="hidden" name="operacio" value="elimina">
							<button class='btn btn-danger' type='submit'>Esborra</button>
						</form>
					</td>
				</tr>
				<?php
			}}
			?>
			</tbody>
		</table>
	</section>
</div>
</body>

</html>