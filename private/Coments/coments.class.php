<?php

class Coment{
    
    public $conn;
    private $idComent;
    private $comentTxt;
    private $idVideogame;
    private $idStudy;
    private $idUser;
    private $comentLikes;
    private $comentDislikes;
    private $comentList = [];

    function __construct($lConn){
        $this->conn = $lConn;
    }

    function set($camp,$valor){
        switch($camp){
            case 'idComent':$this->idComent=$valor;break;
            case 'comentTxt':$this->comentTxt=$valor;break;
            case 'idVideogame':$this->idVideogame=$valor;break;
            case 'idStudy':$this->idStudy=$valor;break;
            case 'idUser':$this->idUser=$valor;break;
            case 'comentLikes':$this->comentLikes=$valor;break;
            case 'comentDislikes':$this->comentDislikes=$valor;break;
            default:echo 'No hi ha el camp';return false;
        }return true;
    }

    function get($camp){
        switch($camp){
            case 'idComent':return $this->idComent;break;
            case 'comentTxt':return $this->comentTxt;break;
            case 'idVideogame':return $this->idVideogame;break;
            case 'idStudy':return $this->idStudy;break;
            case 'idUser':return $this->idUser;break;
            case 'comentLikes':return $this->comentLikes;break;
            case 'comentDislikes':return $this->comentDislikes;break;
            default: echo 'No hi ha el camp'; return false;
        }return true;
    }

    function llista($filtre=0,$ordre = 0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $tmpSql = 'SELECT * FROM coments '. $filtre ;
        if($ordre)$tmpSql .= ' ORDER BY ' . $ordre ." DESC";
        $SQL = $this->conn->prepare($tmpSql);
		$SQL->execute();
        $this->comentList=$SQL->fetchAll();
        return($this->comentList);
    } 

    function insertComent(){
        $insertador = $this->conn->prepare("INSERT INTO coments (comentTxt,idVideogame,idStudy,idUser) VALUES ('".$this->comentTxt."','".$this->idVideogame."','".$this->idStudy."','".$this->idUser."');");
        $insertador->execute();
        return true;
    }

    function deleteComent(){
        $deleteador = $this->conn->prepare("DELETE FROM coments WHERE idComent=".$this->idComent."");
        if($deleteador->execute()){
            $this->deleteRel('idComent='. $this->idComent);
            return true;
        }else{
            return false;
        }
    }

    function updateComent(){
        $modificador = $this->conn->prepare("UPDATE coments SET idVideogame = '$this->idVideogame' WHERE idComent = $this->idComent");
        $modificador->execute();
    }

    function insertLikes($filtre=0){
        $SQL1 = $this->conn->prepare("SELECT coments.comentLikes FROM coments WHERE idComent=$this->idComent");
        $SQL1->execute();
        $likes = $SQL1->fetchColumn();
        $this->comentLikes = $likes+1;
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = $this->conn->prepare("UPDATE coments SET comentLikes = $this->comentLikes $filtre");
        $SQL->execute();
        return true;
    }

    function insertDislikes($filtre=0){
        $SQL1 = $this->conn->prepare("SELECT coments.comentDislikes FROM coments WHERE idComent=$this->idComent");
        $SQL1->execute();
        $likes = $SQL1->fetchColumn();
        $this->comentDislikes = $likes+1;
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = $this->conn->prepare("UPDATE coments SET comentDislikes = $this->comentDislikes $filtre");
        $SQL->execute();
        return true;
    }

    function insertRel(){
        $SQL = $this->conn->prepare("INSERT INTO relVotesComents (idUser,idComent) VALUES ('".$this->idUser."','".$this->idComent."')");
        if($SQL->execute()){
            return true;
        }else{
            echo 'ERROR';
            die;
        }
    }

    function deleteRel($filtre = 0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL= $this->conn->prepare("DELETE FROM relVotesComents $filtre");
        if($SQL->execute()){
            return true;
        }else{
            return false;
        }
    }

    function llistaRel($filtre = 0){
        $a_votesComentstmp =[];
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = $this->conn->prepare("SELECT * FROM relVotesComents $filtre");
        if($SQL->execute()){
            $a_votesComentstmp = $SQL->fetchAll();
            return $a_votesComentstmp;
        }else{
            return false;
        }
    }
    
}