<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Panel administracion</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0; width=device-width;">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="../scss/style.scss">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>

<section>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid d-flex">
			<a class="navbar-brand" href="../../index.php">VideoJuan</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class="navbar-nav">
					<a href="../Videogames/videogames.php" class="nav-link m-2">Videogames</i></a>
					<a href="../Categories/categories.php" class="nav-link m-2">Categories</i></a>
					<a href="../Platforms/platforms.php" class="nav-link m-2">Platforms</i></a>
					<a href="../Retailers/retailers.php" class="nav-link m-2">Retailers</i></a>
					<a href="../Studies/studies.php" class="nav-link m-2">Studies</i></a>
					<a href="../Videos/videos.php" class="nav-link m-2">Videos</i></a>
					<a href="../Coments/coments.php" class="nav-link m-2">Coments</i></a>
					<a href="#" class="nav-link m-2 ms-5"><?php echo $_SESSION["usuari"]; ?></a>
					<form class="nav-link" action="../login.php">
						<input type="hidden" name="accio" value="sortir" />
						<button class="btn btn-warning btn-sm" type="submit"><i class="icon-2x icon-signout"></i></button>
					</form>
				</div>
			</div>
		</div>
	</nav>
</section>