<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
}

$operacio = $_REQUEST['operacio'];
$id = $_REQUEST['idCategory'];
$categoryName = $_REQUEST['categoryName'];
$myCategory = new Category($conn);

switch($operacio){
    case 'elimina':
        $myCategory->deleteCategory($_REQUEST['deleterid']);
        break;
    case 'modificar':
        $myCategory->updateCategory($categoryName, $id);
        break;
    case 'insertar':
        $myCategory->insertCategory($categoryName);
        break;
}header("Location: categories.php");

?>
