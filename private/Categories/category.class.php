<?php
class Category{
    
    public $conn;
    private $categoryList = [];

    function __construct($lConn){
        $this->conn = $lConn;
    }

    function llista($filtre=0){
        if(!$filtre)$filtre='';
        else $filtre = ' WHERE ' . $filtre;
        $laMevaSentencia = $this->conn->prepare("SELECT * FROM categories $filtre");
		$laMevaSentencia->execute();
        $this->categoryList=$laMevaSentencia->fetchAll();
        return($this->categoryList);
    } 

    function insertCategory($categoryName){
        $insertador = $this->conn->prepare("INSERT INTO categories (categoryName) VALUES ('".$categoryName."');");
        $insertador->execute();
    }

    function deleteCategory($id){
        $deleteador = $this->conn->prepare("DELETE FROM categories WHERE idCategory=".$id."");
        $deleteador->execute();
    }

    function updateCategory($categoryName,$id){
        $modificador = $this->conn->prepare("UPDATE categories SET categoryName = '$categoryName' WHERE idCategory = $id");
        $modificador->execute();
    }
    
}