<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

$myCategory = new Category($conn);
?>
<html>
    <body>
    <?php
    $a_myCategory = [];
    if($a_myCategory = $myCategory->llista()){
        foreach($a_myCategory as $myCategoryTMP){
            if($myCategoryTMP['idCategory'] == $_REQUEST['idCategory']){
    ?>
    <section>
        <div class="container p-5">
            <form action="categoriesOperacions.php">
                <input type="hidden" name="idCategory" value="<?= $myCategoryTMP['idCategory'] ?>">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="nom" class="textmuted h8">Nom</label>
                    <input type="text" class="form-control" name="categoryName" id="categoryName" value="<?= $myCategoryTMP['categoryName'] ?>" required>
                </div>
            </div>
            <input type="hidden" name="operacio" value="modificar">
            <button type="submit" class="btn btn-danger">Submit</button>
            </form>
        </div>
    </section>
    <?php }}}?>
</body>
</html>