<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

$myCategory = new Category($conn);
?>

<body>
<h1 class="text-center mt-2">CATEGORIES</h1>
<div class="container-fluid p-5">
	<section>	
	<a href="addCategory.php" class='btn btn-secondary m-2'>Crea un categoria nova</a>
	</section>
	<section>
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Nom</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$a_myCategory = [];
			if($a_myCategory = $myCategory->llista()){
				foreach($a_myCategory as $myCategoryTMP){
				?>
				<tr>
					<th scope="row"><?=$myCategoryTMP['idCategory']?></th>
					<td class="col-8"><?=$myCategoryTMP['categoryName']?></td>
					<td>
						<form method='POST' action='modifyCategory.php'>
							<input type='hidden' name='idCategory' value="<?=$myCategoryTMP['idCategory']?>" >
							<button class='btn btn-secondary' type='submit'> Edit </button>
						</form>
					</td>
					<td>
						<form method='POST' action='categoriesOperacions.php'>
							<input type='hidden' name='deleterid' value="<?=$myCategoryTMP['idCategory']?>">
							<input type="hidden" name="operacio" value="elimina">
							<button class='btn btn-danger' type='submit'>Delete</button>
						</form>
					</td>
				</tr>
				<?php
			}}
			?>
			</tbody>
		</table>
	</section>
</div>
</body>

</html>