<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if(!isset($_SESSION)) {session_start();};

if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
}

$operacio = $_REQUEST['operacio'];
$id = $_REQUEST['idPlatform'];
$platformName = $_REQUEST['platformName'];
$myPlatform = new Platform($conn);

switch($operacio){
    case 'elimina':
        $myPlatform->deletePlatform($_REQUEST['deleterid']);
        break;
    case 'modificar':
        echo "ID = ".$id;
        $myPlatform->updatePlatform($platformName,$id);
        break;
    case 'insertar':
        $myPlatform->insertPlatform($platformName);
        break;
}header("Location: platforms.php");

?>
