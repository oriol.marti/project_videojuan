<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
if(!isset($_SESSION)) {session_start();};

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

$myPlatform = new Platform($conn);
?>

<body>
	<h1 class="text-center mt-2">PLATAFORMES</h1>
<div class="container-fluid p-5">
	<section>	
	<a href="addplatform.php" class='btn btn-secondary m-2'>Crea una Plataforma nova</a>
	</section>
	<section>
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Nom</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$a_myPlatform = [];
			if($a_myPlatform = $myPlatform->llista()){
				foreach($a_myPlatform as $myPlatformTMP){
				?>
				<tr>
					<th scope="row"><?=$myPlatformTMP['idPlatform']?></th>
					<td class="col-8"><?=$myPlatformTMP['platformName']?></td>
					<td>
						<form method='POST' action='modifyplatform.php'>
							<input type='hidden' name='idPlatform' value="<?=$myPlatformTMP['idPlatform']?>" >
							<button class='btn btn-secondary' type='submit'> Edit </button>
						</form>
					</td>
					<td>
						<form method='POST' action='platformOperacions.php'>
							<input type='hidden' name='deleterid' value="<?=$myPlatformTMP['idPlatform']?>">
							<input type="hidden" name="operacio" value="elimina">
							<button class='btn btn-danger' type='submit'>Delete</button>
						</form>
					</td>
				</tr>
				<?php
			}}
			?>
			</tbody>
		</table>
	</section>
</div>
</body>

</html>