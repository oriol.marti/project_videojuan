<?php

class Platform{
    
    public $conn;
    private $platformList = [];

    function __construct($lConn){
        $this->conn = $lConn;
    }

    function llista(){
        $laMevaSentencia = $this->conn->prepare("SELECT * FROM platforms");
		$laMevaSentencia->execute();
        $this->platformList=$laMevaSentencia->fetchAll();
        return($this->platformList);
    } 

    function insertPlatform($platformName){
        $insertador = $this->conn->prepare("INSERT INTO platforms (platformName) VALUES ('".$platformName."');");
        $insertador->execute();
    }

    function deletePlatform($id){
        $deleteador = $this->conn->prepare("DELETE FROM platforms WHERE idPlatform=".$id."");
        $deleteador->execute();
    }

    function updatePlatform($platformName,$id){
        $modificador = $this->conn->prepare("UPDATE platforms SET platformName = '$platformName' WHERE idPlatform = $id");
        $modificador->execute();
    }
    
}