<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
}

$operacio = $_REQUEST['operacio'];
$idStudy = $_REQUEST['idStudy'];
$studyName = addSlashes($_REQUEST['studyName']);
$studyBio = addSlashes($_REQUEST['studyBio']);
$studyRating = $_REQUEST['studyRating'];
$public = $_REQUEST['public'];
$idUser = $_REQUEST['idUser'];
$myStudy = new Study($conn);

switch($operacio){
    case 'elimina':
        $myStudy->deleteStudy($_REQUEST['deleterid']);
        header("Location: studies.php");
        break;
    case 'modificar':
        $myStudy->updateStudy($studyName,$studyBio,$idStudy,$studyRating);
        header("Location: studies.php");
        break;
    case 'insertar':
        $myStudy->insertStudy($studyName,$studyBio);
        header("Location: studies.php");
        break;
    case 'Like':
        if($myStudy->set('idStudy', $idStudy) && $myStudy->set('user',$idUser))
            $myStudy->insertLikes('idStudy= ' .$idStudy);
            $myStudy->insertRel('votes');
            if($public == 'public'){
                header("Location: ../../study_details.php?value=".$idStudy);
            }else {
                header("Location: ../../studiesLlista.php");
            }
        break;
    case 'Dislike':
        if($myStudy->set('idStudy', $idStudy) && $myStudy->set('user',$idUser))
            $myStudy->insertDislikes('idStudy= ' .$idStudy);
            $myStudy->insertRel('votes');
            if($public == 'public'){
                header("Location: ../../study_details.php?value=".$idStudy);
            }else {
                header("Location: ../../studiesLlista.php");
            }
        break;
}//header("Location: studies.php");

?>

