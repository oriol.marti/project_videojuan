<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

$myStudy = new Study($conn);
?>
<html>
    <body>
    <?php
    $a_myStudy = [];
    if($a_myStudy = $myStudy->llista()){
        foreach($a_myStudy as $myStudyTMP){
            if($myStudyTMP['idStudy'] == $_REQUEST['idStudy']){
    ?>
    <section>
        <div class="container p-5">
            <form action="studiesOperacions.php">
                <input type="hidden" name="idStudy" value="<?= $myStudyTMP['idStudy'] ?>">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">Nom</label>
                    <input type="text" class="form-control" name="studyName" id="studyName" value="<?= $myStudyTMP['studyName'] ?>" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">Rating</label>
                    <select class="form-control" name="studyRating" id="">
                        <option value="0" <?php if($myStudyTMP['studyRating'] == 0)echo 'selected';?>>0*</option>
                        <option value="1" <?php if($myStudyTMP['studyRating'] == 1)echo 'selected';?>>1*</option>
                        <option value="2" <?php if($myStudyTMP['studyRating'] == 2)echo 'selected';?>>2*</option>
                        <option value="3" <?php if($myStudyTMP['studyRating'] == 3)echo 'selected';?>>3*</option>
                        <option value="4" <?php if($myStudyTMP['studyRating'] == 4)echo 'selected';?>>4*</option>
                        <option value="5" <?php if($myStudyTMP['studyRating'] == 5)echo 'selected';?>>5*</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="bio" class="textmuted h8">Bio del distribuidor</label>
                    <textarea rows="5" class="form-control" name="studyBio" id="studyBio" required><?= $myStudyTMP['studyBio']?></textarea>
                </div>
            </div>
            <input type="hidden" name="operacio" value="modificar">
            <button type="submit" class="btn btn-danger">Submit</button>
            </form>
        </div>
    </section>
    <?php }}}?>
</body>
</html>