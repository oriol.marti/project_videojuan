<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

?>

    <section>
        <div class="container p-5">
            <form action="studiesOperacions.php">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="nom" class="textmuted h8">Nom del Distribuidor</label>
                    <input type="text" class="form-control ms-1" name="studyName" id="studyName" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="Bio" class="textmuted h8">Bio del Distribuidor</label>
                    <textarea class="form-control" name="studyBio" id="studyBio" required></textarea>
                </div>
            </div>
            <input type="hidden" name="operacio" value="insertar">
            <button type="submit" class="btn btn-danger">Submit</button>
            </form>
        </div>
    </section>
</body>