<?php

class Study{

    public $conn;
    private $idStudy;
    private $studyName;
    private $studyLikes;
    private $studyDislikes;
    private $user;
    private $studyList = [];

    function __construct($lConn){
        $this->conn = $lConn;
    }

    function set($camp,$valor){
        switch($camp){
            case 'idStudy':$this->idStudy=$valor;break;
            case 'studyName':$this->studyName=$valor;break;
            case 'studyLikes':$this->studyLikes=$valor;break;
            case 'studyDislikes':$this->studyDislikes=$valor;break;
            case 'user':$this->user=$valor;break;
            
            default:echo 'No hi ha el camp';return false;
        }return true;
    }

    function get($camp){
        switch($camp){
            case 'idStudy':return $this->idStudy;break;
            case 'studyName':return $this->studyName;break;
            case 'studyLikes':return $this->studyLikes;break;
            case 'studyDislikes':return $this->studyDislikes;break;
            case 'user':return $this->user;break;

            default: echo 'No hi ha el camp'; return false;
        }return true;
    }
    
    
    function llista($filtre=0,$limit=0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        if($limit) $limit = ' LIMIT ' . $limit;
        else $limit = '';
        $SQL = "SELECT * FROM studies $filtre $limit";
        $laMevaSentencia = $this->conn->prepare($SQL);
		$laMevaSentencia->execute();
        $this->videogameList=$laMevaSentencia->fetchAll();
        return($this->videogameList);
    }

    function insertStudy($studyName,$studyBio){
        $insertador = $this->conn->prepare("INSERT INTO studies (studyName,studyBio) VALUES ('".$studyName."','".$studyBio."');");
        $insertador->execute();
    }

    function deleteStudy($id){
        $deleteador = $this->conn->prepare("DELETE FROM studies WHERE idStudy=".$id."");
        $deleteador->execute();
    }

    function updateStudy($studyName,$studyBio,$id,$studyRating){
        $modificador = $this->conn->prepare("UPDATE studies SET studyName = '$studyName', studyBio = '$studyBio', studyRating = '$studyRating' WHERE idStudy = $id");
        $modificador->execute();
    }

    function llistaRel($camp,$filtre = 0){
        switch($camp){
            case 'votes':
                $a_votesTmp =[];
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL = $this->conn->prepare("SELECT * FROM relVotes $filtre");
                if($SQL->execute()){
                    $a_votesTmp = $SQL->fetchAll();
                    return $a_votesTmp;
                }else{
                    return false;
                }
            break;
        }
    }

    function insertRel($camp){
        switch($camp){    
            case 'votes':
                $SQL = $this->conn->prepare("INSERT INTO relVotes (idUser,idStudy) VALUES ('".$this->user."','".$this->idStudy."')");
                if($SQL->execute()){
                    return true;
                }else{
                    echo 'ERROR';
                    die;
                }
            break;
        }
    }

    function insertLikes($filtre=0){
        $SQL1 = $this->conn->prepare("SELECT studies.studyLikes FROM studies WHERE idStudy=$this->idStudy");
        $SQL1->execute();
        $likes = $SQL1->fetchColumn();
        $this->studyLikes = $likes+1;
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = $this->conn->prepare("UPDATE studies SET studyLikes = $this->studyLikes $filtre");
        $SQL->execute();
        return true;
    }

    function insertDislikes($filtre=0){
        $SQL1 = $this->conn->prepare("SELECT studies.studyDislikes FROM studies WHERE idStudy=$this->idStudy");
        $SQL1->execute();
        $likes = $SQL1->fetchColumn();
        $this->studyDislikes = $likes+1;
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = $this->conn->prepare("UPDATE studies SET studyDislikes = $this->studyDislikes $filtre");
        $SQL->execute();
        return true;
    }

    function numTotal($filtre=0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = "SELECT COUNT(idStudy) FROM studies $filtre";
        $query = $this->conn->prepare($SQL);
        $query->execute();
        $resultat= $query->fetchColumn();
        return $resultat;
    }
    
}