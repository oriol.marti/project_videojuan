<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//session_start();

/* if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
} */

$puntuacio = $_COOKIE['puntuacio'];
$a_categories = [];
$a_categories = $_REQUEST['category'];
$a_platforms = $_REQUEST['platform'];
$a_retailers = $_REQUEST['retailer'];
$operacio = $_REQUEST['operacio'];
$videogameRating = $_REQUEST['videogameRating'];
$idVideogame = $_REQUEST['idVideogame'];
$idScreenshot = $_REQUEST['idScreenshot'];
$public = $_REQUEST['public'];
$flappy = $_REQUEST['flappy'];
$tower = $_REQUEST['tower'];
$videogameName = addslashes($_REQUEST['videogameName']);
$videogameLogo = addslashes($_REQUEST['videogameLogo']);
$videogameSynopsis = addslashes($_REQUEST['videogameSynopsis']);
$videogameYear = addslashes($_REQUEST['videogameYear']);
$idStudy = addslashes($_REQUEST['idStudy']);
$fileNameTmp = basename($_FILES["file"]["name"]);
$idUser = $_REQUEST['idUser'];

$myVideogame = new Videogame($conn);

switch($operacio){
    case 'puntuacio':
        if($myVideogame->set('puntuacio',$puntuacio) && $myVideogame->set('user',$idUser)){
            if($flappy){
                if($myVideogame->insertPuntuacio('flappy')){
                    header("Location: ../../jocs/JOC2/flappyBird.php");
                }else{
                    echo 'error insert';
                    die;
                }    
            }elseif($tower){
                if($myVideogame->insertPuntuacio('tower')){
                    header("Location: ../../jocs/JOC3/tower.php");
                }else{
                    echo 'error insert';
                    die;
                } 
            }else{
                if($myVideogame->insertPuntuacio()){
                    header("Location: ../../jocs/JOC1/index.php");
                }else{
                    echo 'error insert';
                    die;
                }
            }
        }
        break;
    case 'elimina':
        if($myVideogame->set('idVideogame', $idVideogame))
            if($myVideogame->deleteVideogame()){
                $myVideogame->deleteRel('categories', 'idVideogame= ' .$idVideogame);
                $myVideogame->deleteRel('platforms', 'idVideogame= ' .$idVideogame);
                $myVideogame->deleteRel('retailers', 'idVideogame= ' .$idVideogame);
                header("Location: videogames.php");
            }
        break;
    case 'modificar':
        if($myVideogame->set('idVideogame', $idVideogame) && $myVideogame->set('videogameName', $videogameName) && $myVideogame->set('videogameLogo', $videogameLogo) && $myVideogame->set('videogameSynopsis', $videogameSynopsis) && $myVideogame->set('idStudy', $idStudy) && $myVideogame->set('fileNameTmp', $fileNameTmp) && $myVideogame->set('categories', $a_categories) && $myVideogame->set('platforms', $a_platforms) && $myVideogame->set('retailers', $a_retailers) && $myVideogame->set('videogameYear', $videogameYear) && $myVideogame->set('videogameRating', $videogameRating))
            if($myVideogame->updateVideogame()){
                $myVideogame->insertRel('categories');
                $myVideogame->insertRel('platforms');
                $myVideogame->insertRel('retailers');
                $myVideogame->insertRating('idVideogame= ' .$idVideogame);
                header("Location: modificador.php?idVideogame=" .$idVideogame);
            };
        break;
    case 'insertar':
        if($myVideogame->set('videogameName', $videogameName) && $myVideogame->set('videogameLogo', $videogameLogo) && $myVideogame->set('videogameSynopsis', $videogameSynopsis) && $myVideogame->set('idStudy', $idStudy) && $myVideogame->set('fileNameTmp', $fileNameTmp))
            $myVideogame->insertVideogame();
            $lastId = $myVideogame->get('idVideogame');
            header("Location: modificador.php?idVideogame=" .$lastId);
        break;
    case 'Like':
        if($myVideogame->set('idVideogame', $idVideogame))
            $myVideogame->insertLikes('idVideogame= ' .$idVideogame);
            $myVideogame->set('user',$idUser);
            $myVideogame->insertRel('votes');
            if($public == 'public'){
                header("Location: ../../game_details.php?value=".$idVideogame);
            }else {
                header("Location: ../../videogamesLlista.php");
            }
        break;
    case 'Dislike':
        if($myVideogame->set('idVideogame', $idVideogame))
            $myVideogame->insertDislikes('idVideogame= ' .$idVideogame);
            $myVideogame->set('user',$idUser);
            $myVideogame->insertRel('votes');
            if($public == 'public'){
                header("Location: ../../game_details.php?value=".$idVideogame);
            }else {
                header("Location: ../../videogamesLlista.php");
            }
        break;
    case 'screenshot':
        if($myVideogame->set('idVideogame', $idVideogame) && $myVideogame->set('fileNameTmp', $fileNameTmp)){
            $myVideogame->insertScreenshot($idUser);
                header("Location: ../../game_details.php?value=".$idVideogame);
            }else{
                echo 'ERROR al pujar screenshot';
                die;
            }
        break;
    case 'deleteScreenshot':
        if($myVideogame->set('idVideogame', $idVideogame) && $myVideogame->set('idScreenshot', $idScreenshot))
        if($myVideogame->deleteScreenshot('idScreenshot= ' .$idScreenshot)){
            header("Location: modificador.php?idVideogame=" .$idVideogame);
        }else{
            echo 'ERROR al pujar screenshot';
            die;
        }
        break;

    default:
        $msgError = 'La operacio no existeix';
        return $msgError;

}//header("Location: videogames.php");
?>
