<?php

class Videogame{
    
    public $conn;
    private $idVideogame;
    private $videogameName;
    private $videogameLogo;
    private $videogameSynopsis;
    private $videogameYear;
    private $idStudy;
    private $fileNameTmp;
    private $idScreenshot;
    private $screenshotUrl;
    private $videogameLikes;
    private $videogameDislikes;
    private $videogameRating;
    private $a_categories=[];
    private $a_platforms=[];
    private $a_retailers=[];
    private $user;
    private $puntuacio;
    private $videogameList = [];
    private $screenshotList = [];

    function __construct($lConn){
        $this->conn = $lConn;
    }

    function set($camp,$valor){
        switch($camp){
            case 'idVideogame': $this->idVideogame = $valor;break;
            case 'videogameName': $this->videogameName = $valor;break;
            case 'videogameLogo': $this->videogameLogo = $valor;break;
            case 'videogameSynopsis': $this->videogameSynopsis = $valor;break;
            case 'videogameYear': $this->videogameYear = $valor;break;
            case 'idStudy': $this->idStudy = $valor;break;
            case 'fileNameTmp': $this->fileNameTmp = $valor;break;
            case 'idScreenshot': $this->idScreenshot = $valor;break;
            case 'screenshotUrl': $this->screenshotUrl = $valor;break;
            case 'videogameLikes': $this->videogameLikes = $valor;break;
            case 'videogameDislikes': $this->videogameDislikes = $valor;break;
            case 'videogameRating': $this->videogameRating = $valor;break;
            case 'lastId': $this->lastId = $valor;break;
            case 'categories': array_push($this->a_categories, $valor);break;
            case 'platforms': array_push($this->a_platforms, $valor);break;
            case 'retailers': array_push($this->a_retailers, $valor);break;
            case 'user': $this->user = $valor;break;
            case 'puntuacio': $this->puntuacio = $valor;break;
            
            default: echo 'No hi ha el camp';return false;
        }return true;
    }

    function get($camp){
        switch($camp){
            case 'idVideogame': return $this->idVideogame;break;
            case 'videogameName': return $this->videogameName;break;
            case 'videogameLogo': return $this->videogameLogo;break;
            case 'videogameSynopsis': return $this->videogameSynopsis;break;
            case 'videogameYear': return $this->videogameYear;break;
            case 'idStudy': return $this->idStudy;break;
            case 'fileNameTmp': return $this->fileNameTmp;break;
            case 'idScreenshot': return $this->idScreenshot;break;
            case 'screenshotUrl': return $this->screenshotUrl;break;
            case 'categories': return $this->a_categories;break;
            case 'videogameLikes': return $this->videogameLikes;break;
            case 'videogameDislikes': return $this->videogameDislikes;break;
            case 'videogameRating': return $this->videogameRating;break;
            case 'lastId': return $this->a_lastId;break;
            case 'platforms': return $this->a_platforms;break;
            case 'retailers': return $this->a_retailers;break;
            case 'user': return $this->user;break;
            case 'puntuacio': return $this->puntuacio;break;

            default: echo 'No hi ha el camp';return false;
        }return true;
    }


    function llista($filtre=0,$limit=0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        if($limit) $limit = ' LIMIT ' . $limit;
        else $limit = '';
        $SQL = "SELECT * FROM videogames $filtre $limit";
        $laMevaSentencia = $this->conn->prepare($SQL);
		$laMevaSentencia->execute();
        $this->videogameList=$laMevaSentencia->fetchAll();
        return($this->videogameList);
    }

    function numTotal($filtre=0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = "SELECT COUNT(idVideogame) FROM videogames $filtre";
        $query = $this->conn->prepare($SQL);
        $query->execute();
        $resultat= $query->fetchColumn();
        return $resultat;
    }

    function llistaScreenshot($filtre=0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $laMevaSentencia = $this->conn->prepare("SELECT * FROM screenshots $filtre");
		$laMevaSentencia->execute();
        $this->screenshotList=$laMevaSentencia->fetchAll();
        return($this->screenshotList);
    }

    function insertVideogame(){
        $insertador = $this->conn->prepare("INSERT INTO videogames (videogameName, videogameLogo, videogameSynopsis, idStudy) VALUES ('".$this->videogameName."', '".$this->videogameLogo."', '".$this->videogameSynopsis."','$this->idStudy');");
        $insertador->execute();
        $last_id = $this->conn->lastInsertId();
        $this->idVideogame = $last_id;
        if($this->fileNameTmp != ''){
            // //echo $this->fileNameTmp;
            // $ultimId = $this->conn->query("SELECT idVideogame FROM videogames ORDER BY idVideogame DESC LIMIT 1;");
            // $test = $ultimId->fetchAll();
            // foreach($test as $tests){
            //     $this->idVideogame = $tests['idVideogame'];
            // };
            if($this->insertScreenshot()){        
            }
        }return true;
    }

    function deleteVideogame(){
        $filtre = 'idVideogame= ' . $this->idVideogame;
        $deleteador = $this->conn->prepare("DELETE FROM videogames WHERE idVideogame=$this->idVideogame");
        $deleteador->execute();
        $this->deleteScreenshot($filtre);
        return true;
    }
    /** 
     * 
     * AQUESTAS FUNCIONS DE MOMENT NOMES INSERTAN LIKES SENSE COMPROVAR RES,
     * A LA PART PUBLICA S'HAURA DE VERIFICAR SI S'ESTA LOGUEJAT I SI S'HA VOTAT JA.
     * 
     */

    function insertLikes($filtre=0){
        $SQL1 = $this->conn->prepare("SELECT videogames.videogameLikes FROM videogames WHERE idVideogame=$this->idVideogame");
        $SQL1->execute();
        $likes = $SQL1->fetchColumn();
        $this->videogameLikes = $likes+1;
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = $this->conn->prepare("UPDATE videogames SET videogameLikes = $this->videogameLikes $filtre");
        if($SQL->execute()){
            return true;
        };
    }

    function insertDislikes($filtre=0){
        $SQL1 = $this->conn->prepare("SELECT videogames.videogameDislikes FROM videogames WHERE idVideogame=$this->idVideogame");
        $SQL1->execute();
        $likes = $SQL1->fetchColumn();
        $this->videogameDislikes = $likes+1;
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = $this->conn->prepare("UPDATE videogames SET videogameDislikes = $this->videogameDislikes $filtre");
        $SQL->execute();
        return true;
    }

    /**
     *  FINS AQUI
     */

    function insertRating($filtre=0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $SQL = $this->conn->prepare("UPDATE videogames SET videogameRating = $this->videogameRating $filtre");
        $SQL->execute();
        return true;
    }

    function deleteScreenshot($filtre=0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        if(strpos($filtre,'idScreenshot')){
            $fileName = $this->conn->prepare("SELECT * FROM screenshots $filtre");
            $fileName->execute();
            while($myScreenshot = $fileName->fetchObject()){
            unlink('../uploads/'.$myScreenshot->screenshotUrl);
            }
        }
        $deleteador = $this->conn->prepare("DELETE FROM screenshots $filtre");
        if($deleteador->execute()){
            return true;
        }else{
            return false;
        }
    }

    function updateVideogame(){
        $modificador = $this->conn->prepare("UPDATE videogames SET videogameName = '$this->videogameName', videogameLogo = '$this->videogameLogo', videogameSynopsis = '$this->videogameSynopsis', idStudy = $this->idStudy, videogameYear = '$this->videogameYear' WHERE idVideogame = $this->idVideogame");
        if($modificador->execute()){
            $this->insertScreenshot();
            return true;
        }else{
            return false;
        }
    }

    function llistaRel($camp,$filtre = 0){
        switch($camp){
            case 'categories':
                $a_categoriestmp =[];
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL = $this->conn->prepare("SELECT * FROM relCategories $filtre");
                if($SQL->execute()){
                    $a_categoriestmp = $SQL->fetchAll();
                    return $a_categoriestmp;
                }else{
                    return false;
                }
            break;
            case 'platforms':
                $a_platformsTmp =[];
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL = $this->conn->prepare("SELECT * FROM relPlatforms $filtre");
                if($SQL->execute()){
                    $a_platformsTmp = $SQL->fetchAll();
                    return $a_platformsTmp;
                }else{
                    return false;
                }
            break;
            case 'retailers':
                $a_retailersTmp =[];
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL = $this->conn->prepare("SELECT * FROM relRetailers $filtre");
                if($SQL->execute()){
                    $a_retailersTmp = $SQL->fetchAll();
                    return $a_retailersTmp;
                }else{
                    return false;
                }
            break;
            case 'votes':
                $a_votesTmp =[];
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL = $this->conn->prepare("SELECT * FROM relVotes $filtre");
                if($SQL->execute()){
                    $a_votesTmp = $SQL->fetchAll();
                    return $a_votesTmp;
                }else{
                    return false;
                }
            break;
        }
    }

    function deleteRel($camp,$filtre=0){
        switch($camp){
            case 'categories':
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL= $this->conn->prepare("DELETE FROM relCategories $filtre");
                if($SQL->execute()){
                    return true;
                }else{
                    return false;
                }
            break;
            case 'platforms':
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL= $this->conn->prepare("DELETE FROM relPlatforms $filtre");
                if($SQL->execute()){
                    return true;
                }else{
                    return false;
                }
            break;
            case 'retailers':
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL= $this->conn->prepare("DELETE FROM relRetailers $filtre");
                if($SQL->execute()){
                    return true;
                }else{
                    return false;
                }
            break;
            case 'votes':
                if(!$filtre)$filtre='';
                else $filtre = 'WHERE ' . $filtre;
                $SQL= $this->conn->prepare("DELETE FROM relVotes $filtre");
                if($SQL->execute()){
                    return true;
                }else{
                    return false;
                }
            break;
            
            default:
            echo 'No hi ha el camp';
            return false;
        }
    }

    function insertRel($camp){
        switch($camp){    
            case 'categories':
            //echo count($this->a_categories);
            //var_dump($this->a_categories);
            $this->deleteRel('categories','idVideogame='.$this->idVideogame);
            foreach($this->a_categories as $categories){
                foreach($categories as $id){
                    $SQL = $this->conn->prepare("INSERT INTO relCategories (idCategory,idVideogame) VALUES ($id,'".$this->idVideogame."')");
                    if($SQL->execute());
                }
            }
            break;
            case 'platforms':
                $this->deleteRel('platforms','idVideogame='.$this->idVideogame);
                foreach($this->a_platforms as $platforms){
                    foreach($platforms as $id){
                        $SQL = $this->conn->prepare("INSERT INTO relPlatforms (idPlatform,idVideogame) VALUES ($id,'".$this->idVideogame."')");
                        $SQL->execute();
                    }
                }
            break;
            case 'retailers':
                $this->deleteRel('retailers','idVideogame='.$this->idVideogame);
                foreach($this->a_retailers as $retailers){
                    foreach($retailers as $id){
                        $SQL = $this->conn->prepare("INSERT INTO relRetailers (idRetailer,idVideogame) VALUES ($id,'".$this->idVideogame."')");
                        $SQL->execute();
                    }
                }
            case 'votes':
                $SQL = $this->conn->prepare("INSERT INTO relVotes (idUser,idVideogame) VALUES ('".$this->user."','".$this->idVideogame."')");
                if($SQL->execute()){
                    return true;
                }else{
                    echo 'ERROR';
                    die;
                }
            break;
        }
    }

    function insertPuntuacio($camp=0){
        switch($camp){
            case 'flappy':
                $SQL2 = "SELECT puntuacioJoc2 FROM users WHERE idUser = $this->user";
                $query2 = $this->conn->prepare($SQL2); 
                echo $SQL2;
                if($query2->execute()){
                    $resultat= $query2->fetchColumn();
                    if($resultat>$this->puntuacio){
                        return true;
                    }else{
                        $SQL = "UPDATE users SET puntuacioJoc2 = '$this->puntuacio' WHERE idUser = $this->user";
                        echo $SQL;
                        $query = $this->conn->prepare($SQL);
                        if($query->execute()){
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
                break;
            case 'tower':
                $SQL2 = "SELECT puntuacioJoc3 FROM users WHERE idUser = $this->user";
                $query2 = $this->conn->prepare($SQL2); 
                echo $SQL2;
                if($query2->execute()){
                    $resultat= $query2->fetchColumn();
                    if($resultat>$this->puntuacio){
                        return true;
                    }else{
                        $SQL = "UPDATE users SET puntuacioJoc3 = '$this->puntuacio' WHERE idUser = $this->user";
                        echo $SQL;
                        $query = $this->conn->prepare($SQL);
                        if($query->execute()){
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
                break;
            default:
                $SQL2 = "SELECT puntuacioJoc1 FROM users WHERE idUser = $this->user";
                $query2 = $this->conn->prepare($SQL2); 
                echo $SQL2;
                if($query2->execute()){
                    $resultat= $query2->fetchColumn();
                    if($resultat>$this->puntuacio){
                        return true;
                    }else{
                        $SQL = "UPDATE users SET puntuacioJoc1 = '$this->puntuacio' WHERE idUser = $this->user";
                        echo $SQL;
                        $query = $this->conn->prepare($SQL);
                        if($query->execute()){
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
                break;
        }
        
    }

    function insertScreenshot($idUser=0){
        // Include the database configuration file
        $statusMsg = '';

        $idVideogame = $this->idVideogame;
        if(!$idUser)$idUser=0;

        // File upload path
        $targetDir = "../uploads/";
        $fileName = $this->fileNameTmp;
        $targetFilePath = $targetDir . $fileName;
        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

        if(!empty($_FILES["file"]["name"])){
            // Allow certain file formats
            $allowTypes = array('jpg','png','jpeg','gif','pdf');
            if(in_array($fileType, $allowTypes)){
                //echo "\n <br> ENTRO 1##";
                // Upload file to server
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    //echo "\n <br> ENTRO 2##";
                    // Insert image file name into database
                    $insert = $this->conn->prepare("INSERT into screenshots (screenshotUrl,idVideogame,idUser) VALUES ('".$fileName."','$idVideogame','$idUser');");
                    //echo "\n <br< " . $insert;
                    $insert->execute();
                    if($insert){
                        //echo "\n <br> ENTRO 3##";
                        $statusMsg = "The file ".$fileName. " has been uploaded successfully.";
                    }else{
                        $statusMsg = "File upload failed, please try again.";
                        echo $statusMsg;
                        return false;
                    }
                }else{
                    $statusMsg = "Sorry, there was an error uploading your file.";
                    echo $statusMsg;
                    return false;
                }
            }else{
                $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.';
                echo $statusMsg;
                return false;
            }
        }else{
            $statusMsg = 'Please select a file to upload.';
            echo $statusMsg;
            return false;
        }

        // Display status message
        echo $statusMsg;
        return true;

    }
            
}