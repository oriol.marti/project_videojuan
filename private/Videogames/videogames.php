<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html');    
}

if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';

$myVideogame = new Videogame($conn);
?>

<body>
	<h1 class="text-center mt-2">VIDEOJOCS</h1>
	<div class="container-fluid p-5">
	<section>	
		<a href="addVideogame.php" class='btn btn-secondary m-2'>Crea un videojoc nou </a>
	</section>
		<section>
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Nom</th>
				</tr>
			</thead>
			<tbody>
		<?php
		$a_myVideogame = [];
		if($a_myVideogame = $myVideogame->llista()){
			foreach($a_myVideogame as $myVideogameTMP){
		?>
				<tr>
					<th scope="row"><?=$myVideogameTMP['idVideogame']?></th>
					<td class="col-8"><?=$myVideogameTMP['videogameName']?></td>
					<td>
						<form method='POST' action='modificador.php'>
							<input type='hidden' name='idVideogame' value="<?=$myVideogameTMP['idVideogame']?>" >
							<button class='btn btn-secondary' type='submit'> Edit </button>
						</form>
					</td>
					<td>
						<form method='POST' action='operacions.php'>
							<input type='hidden' name='idVideogame' value="<?=$myVideogameTMP['idVideogame']?>">
							<input type='hidden' name='operacio' value="elimina">
							<button class='btn btn-danger' type='submit'>Delete</button>
						</form>
					</td>
				</tr>
				<?php 
			}
		}
			?>
			</tbody>
		</table>
	</section>
	</div>
</body>

</html>