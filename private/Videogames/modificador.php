<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';
        $myStudy = new Study($conn);
        $myVideogame = new Videogame($conn);
		$a_myVideogame = [];
        $a_myScreenshot = [];
		if($a_myVideogame = $myVideogame->llista()){
			foreach($a_myVideogame as $myVideogameTMP){
                if($myVideogameTMP['idVideogame'] == $_REQUEST['idVideogame']){
                ?>
                <section>
                <h1 class="text-center mt-2">VIDEOJOCS</h1>
                    <div class="container p-5">
                        <!-- Aixo no va aqui, es nomes una prova del que sera a la part publica -->
                        
                        <!-- ################################################################## -->
                        <form action="operacions.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="idVideogame" value="<?= $myVideogameTMP['idVideogame'] ?>">
                            <input type="hidden" name="operacio" value="modificar">
                            <div class="row mb-5">
                                <div class="logo col-md-6">
                                    <img width="290px" src="<?=$myVideogameTMP['videogameLogo'] ?>">
                                </div>
                                <div class="col-md-6">
                                    <label for="nom" class="textmuted h8">Nom</label>
                                    <input type="text" class="form-control mb-3" name="videogameName" id="videogameName" value="<?=$myVideogameTMP['videogameName'] ?>" required>
                                    <label for="nom" class="textmuted h8">Logo (Introdueix la URL)</label>
                                    <input type="text" class="form-control mb-3" name="videogameLogo" id="videogameLogo" value="<?=$myVideogameTMP['videogameLogo'] ?>" required>
                                    <label for="nom" class="textmuted h8">Any de llançament</label>
                                    <input type="text" class="form-control mb-3" name="videogameYear" id="videogameYear" value="<?=$myVideogameTMP['videogameYear'] ?>" required>
                                    <label for="nom" class="textmuted h8">Rating</label>
                                    <select class="form-control" name="videogameRating" id="">
                                        <option value="0" <?php if($myVideogameTMP['videogameRating'] == 0)echo 'selected';?>>0*</option>
                                        <option value="1" <?php if($myVideogameTMP['videogameRating'] == 1)echo 'selected';?>>1*</option>
                                        <option value="2" <?php if($myVideogameTMP['videogameRating'] == 2)echo 'selected';?>>2*</option>
                                        <option value="3" <?php if($myVideogameTMP['videogameRating'] == 3)echo 'selected';?>>3*</option>
                                        <option value="4" <?php if($myVideogameTMP['videogameRating'] == 4)echo 'selected';?>>4*</option>
                                        <option value="5" <?php if($myVideogameTMP['videogameRating'] == 5)echo 'selected';?>>5*</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="nom" class="textmuted h8">Synopsis</label>
                                    <textarea type="text" rows="5" class="form-control" name="videogameSynopsis" id="videogameSynopsis" required><?=$myVideogameTMP['videogameSynopsis'] ?></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="study" class="form-label">Selecciona el Estudi</label>
                                    <select name="idStudy" class="form-control mb-3">
                                        <?php
                                        $a_myStudy = [];
                                        if($a_myStudy = $myStudy->llista()){
                                            foreach($a_myStudy as $myStudyTMP){
                                            ?>
                                            <option value="<?=$myStudyTMP['idStudy']?>"
                                            <?php
                                            if($myVideogameTMP['idStudy'] == $myStudyTMP['idStudy'])echo 'selected';
                                            ?>>
                                                <?=$myStudyTMP['studyName']?>
                                            </option>
                                            <?php
                                        }}
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label">
                                        Selecciona una imatge per pujar:
                                    </label>
                                    <input class="form-control" type="file" name="file">
                                </div>
                            </div>
                            <section>
                                <div class="row mb-5">
                                    <h4><a class="checkbox" data-bs-toggle="collapse" data-bs-target="#collapseExample">Selecciona Categories</a></h4>
                                    <?php
                                        $myCategory = new Category($conn);
                                        $a_categories = [];
                                        $a_categories = $myVideogame->llistaRel('categories','idVideogame='. $myVideogameTMP['idVideogame']);
                                        $a_myCategory = [];
                                        
                                        if($a_myCategory = $myCategory->llista()){
                                            foreach($a_myCategory as $categories){
                                                ?>
                                                    <div class="col-3 collapse mt-2" id="collapseExample">
                                                        <input type="checkbox" name="category[]" value="<?=$categories['idCategory']?>"
                                                        <?php
                                                        foreach($a_categories as $idsCategories){
                                                            if($categories['idCategory'] == $idsCategories['idCategory']){
                                                                echo 'checked';
                                                            };
                                                        }?>> <?=$categories['categoryName']?>
                                                    </div>
                                                <?php
                                            };
                                        };
                                    ?>
                                </div>
                            </section>
                            <section>
                                <div class="row mb-5">
                                    <h4><a class="checkbox" data-bs-toggle="collapse" data-bs-target="#collapseExample2">Selecciona Plataformes</a></h4>
                                    <?php
                                        $myPlatform = new Platform($conn);
                                        $a_platforms = [];
                                        $a_platforms = $myVideogame->llistaRel('platforms','idVideogame='. $myVideogameTMP['idVideogame']);
                                        $a_myPlatform = [];
                                        
                                        if($a_myPlatform = $myPlatform->llista()){
                                            foreach($a_myPlatform as $platforms){
                                                ?>
                                                    <div class="col-3 collapse mt-2" id="collapseExample2">
                                                        <input type="checkbox" name="platform[]" value="<?=$platforms['idPlatform']?>"
                                                        <?php
                                                        foreach($a_platforms as $idsPlatforms){
                                                            if($platforms['idPlatform'] == $idsPlatforms['idPlatform']){
                                                                echo 'checked';
                                                            };
                                                        }?>> <?=$platforms['platformName']?>
                                                    </div>
                                                <?php
                                            };
                                        };
                                    ?>
                                </div>
                            </section>
                            <section>
                                <div class="row mb-5">
                                    <h4><a class="checkbox" data-bs-toggle="collapse" data-bs-target="#collapseExample3">Selecciona Distribuidors</a></h4>
                                    <?php
                                        $myRetailer = new Retailer($conn);
                                        $a_retailers = [];
                                        $a_retailers = $myVideogame->llistaRel('retailers','idVideogame='. $myVideogameTMP['idVideogame']);
                                        $a_myRetailer = [];
                                        
                                        if($a_myRetailer = $myRetailer->llista()){
                                            foreach($a_myRetailer as $retailers){
                                                ?>
                                                    <div class="col-3 collapse mt-2" id="collapseExample3">
                                                        <input type="checkbox" name="retailer[]" value="<?=$retailers['idRetailer']?>"
                                                        <?php
                                                        foreach($a_retailers as $idsRetailers){
                                                            if($retailers['idRetailer'] == $idsRetailers['idRetailer']){
                                                                echo 'checked';
                                                            };
                                                        }?>> <?=$retailers['retailerName']?>
                                                    </div>
                                                <?php
                                            };
                                        };
                                    ?>
                                </div>
                            </section>
                            <button type="submit" value="Upload" class="btn btn-danger mb-5">Submit</button>
                        </form>
                        <h4 class="mt-3"><a class="checkbox" data-bs-toggle="collapse" data-bs-target="#collapseExample4">Llista de Screenshots</a></h4>
                        <div class="row mt-3 collapse" id="collapseExample4">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Url</th>
                                        <th>Elimina</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $a_myScreenshot = [];
                                    if($a_myScreenshot = $myVideogame->llistaScreenshot()){
                                        foreach($a_myScreenshot as $myScreenshotTMP){
                                            if($myScreenshotTMP['idVideogame'] == $myVideogameTMP['idVideogame']){
                                                ?>
                                                <tr>
                                                    <td><img style="width: 200px;" src="../uploads/<?=$myScreenshotTMP['screenshotUrl']?>"></img></td>
                                                    <td>
                                                        <form method='POST' action='../Videogames/operacions.php'>
                                                            <input type="text" name="idVideogame" value="<?=$myVideogameTMP['idVideogame']?>">
                                                            <input type='hidden' name='idScreenshot' value="<?=$myScreenshotTMP['idScreenshot']?>">
                                                            <input type='hidden' name='operacio' value="deleteScreenshot">
                                                            <button class='btn btn-danger' type='submit'>Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
        <?php }}} ?>
</body>