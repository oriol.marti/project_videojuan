<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';
$myStudy = new Study($conn);
?>


    <!DOCTYPE html>
    <html lang="en">

    <body>
    <h1 class="text-center mt-2">VIDEOJOCS</h1>
    <section>
        <div class="container-fluid p-5">
            <form action="operacions.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="operacio" value="insertar">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="form-label">Nom</label>
                    <input type="text" class="form-control" style="margin-left: 0px;" name="videogameName" id="videogameName" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="nom" class="form-label">videogameLogo</label>
                    <input type="text" class="form-control" style="margin-left: 0px;" name="videogameLogo" id="videogameLogo" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="study" class="form-label">Selecciona el Estudi</label>
                    <select name="idStudy" class="form-control mb-3">
                        <?php
                        $a_myStudy = [];
                        if($a_myStudy = $myStudy->llista()){
                            foreach($a_myStudy as $myStudyTMP){
                            ?>
                            <option value="<?=$myStudyTMP['idStudy']?>">
                                <?=$myStudyTMP['studyName']?>
                            </option>
                            <?php
                        }}
                        ?>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label class="form-label">
                        Selecciona una imatge per pujar:
                    </label>
                    <input class="form-control" type="file" name="file">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="lastName" class="textmuted h8">Synopsis</label>
                    <textarea rows="5" type="text" class="form-control" name="videogameSynopsis" id="videogameSynopsis" placeholder="" value="" required></textarea>
                </div>
            </div>
            <button type="submit" value="Upload" class="btn btn-danger m-2">Submit</button>
            </form>
        </div>
    </section>
</body>