<?php

class Video{
    
    public $conn;
    private $idVideo;
    private $videoUrl;
    private $idVideogame;
    private $videoList = [];

    function __construct($lConn){
        $this->conn = $lConn;
    }

    function set($camp,$valor){
        switch($camp){
            case 'idVideo':$this->idVideo=$valor;break;
            case 'videoUrl':$this->videoUrl=$valor;break;
            case 'idVideogame':$this->idVideogame=$valor;break;
            
            default:echo 'No hi ha el camp';return false;
        }return true;
    }

    function get($camp){
        switch($camp){
            case 'idVideo':return $this->idVideo;break;
            case 'videoUrl':return $this->videoUrl;break;
            case 'idVideogame':return $this->idVideogame;break;

            default: echo 'No hi ha el camp'; return false;
        }return true;
    }

    function llista($filtre=0){
        if(!$filtre)$filtre='';
        else $filtre = 'WHERE ' . $filtre;
        $laMevaSentencia = $this->conn->prepare("SELECT * FROM videos $filtre");
		$laMevaSentencia->execute();
        $this->videoList=$laMevaSentencia->fetchAll();
        return($this->videoList);
    } 

    function insertVideo(){
        $insertador = $this->conn->prepare("INSERT INTO videos (videoUrl,idVideogame) VALUES ('".$this->videoUrl."','".$this->idVideogame."');");
        $insertador->execute();
        return true;
    }

    function deleteVideo(){
        $deleteador = $this->conn->prepare("DELETE FROM videos WHERE idVideo=".$this->idVideo."");
        $deleteador->execute();
    }

    function updateVideo(){
        $modificador = $this->conn->prepare("UPDATE videos SET idVideogame = '$this->idVideogame' WHERE idVideo = $this->idVideo");
        $modificador->execute();
    }
    
}