<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();


//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}

if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';
$myVideo = new Video($conn);
?>

<body>
<div class="container-fluid p-5">
	<section>	
	<a href="addVideo.php" class='btn btn-secondary m-2'>Crea un video nou</a>
	</section>
	<section>
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Video</th>
					<th scope="col">Nom Videojoc</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$a_myVideo = [];
			if($a_myVideo = $myVideo->llista()){
				foreach($a_myVideo as $myVideoTMP){
				?>
				<tr>
					<th scope="row"><?=$myVideoTMP['idVideo']?></th>
					<td class="col-8"><?=$myVideoTMP['videoUrl']?></td>
					<td class="col-8"><?php
					$myVidogame = new Videogame($conn);
					$a_videogames = [];
					if($a_videogames = $myVidogame->llista('idVideogame=' . $myVideoTMP['idVideogame'])){
						foreach($a_videogames as $videogameTMP){
								echo $videogameTMP['videogameName'];
						}
					}
					?></td>
					<td>
						<form method='POST' action='modifyVideo.php'>
							<input type='hidden' name='idVideo' value="<?=$myVideoTMP['idVideo']?>" >
							<button class='btn btn-secondary' type='submit'> Edit </button>
						</form>
					</td>
					<td>
						<form method='POST' action='videosOperacions.php'>
							<input type='hidden' name='deleterid' value="<?=$myVideoTMP['idVideo']?>">
							<input type="hidden" name="operacio" value="elimina">
							<button class='btn btn-danger' type='submit'>Delete</button>
						</form>
					</td>
				</tr>
				<?php
			}}
			?>
			</tbody>
		</table>
	</section>
</div>
</body>

</html>