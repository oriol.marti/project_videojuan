<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: ../autenti.html'  );    
}
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require_once $path.'/private/header.php';
?>
    <section>
        <div class="container-fluid p-5">
            <h1>Afegir Video</h1>
            <form action="videosOperacions.php">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">Url (embed) del video</label>
                    <input type="text" class="form-control ms-1" name="videoUrl" id="videoUrl" required>
                    <input type="hidden" name="operacio" value="insertar">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="idVideogame" class="textmuted h8">Videojoc</label>
                    <select class="form-control ms-1" name="idVideogame" id="idVideogame">
                        <?php
                        $myVideogame = new Videogame($conn);
                        $a_myVideogames=[];
                        if($a_myVideogames=$myVideogame->llista()){
                            foreach($a_myVideogames as $videogameTMP){
                                echo '<option value="'.$videogameTMP['idVideogame'].'">'. $videogameTMP['videogameName']. '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-danger">Submit</button>
            </form>
        </div>
    </section>
</body>