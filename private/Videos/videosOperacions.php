<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
}

$operacio = $_REQUEST['operacio'];
$idVideo = $_REQUEST['idVideo'];
$idVideogame = $_REQUEST['idVideogame'];
$videoUrl = addslashes($_REQUEST['videoUrl']);
$myVideo = new Video($conn);

switch($operacio){
    case 'elimina':
        if($myVideo->set('idVideo',$idVideo)){
            $myVideo->deleteVideo();
        }
        break;
    case 'modificar':
        if($myVideo->set('idVideo', $idVideo) && $myVideo->set('idVideogame', $idVideogame)){
            $myVideo->updateVideo();
        }
        break;
    case 'insertar':
        if($myVideo->set('videoUrl',$videoUrl) && $myVideo->set('idVideogame',$idVideogame)){
            $myVideo->insertVideo();
        }
        break;
}header("Location: videos.php");

?>
