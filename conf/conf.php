<?php

if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conn/conexion.php');
include_once($path.'/user/userService.class.php');
include_once($path.'/private/Videogames/videogames.class.php');
include_once($path.'/private/Videos/video.class.php');
include_once($path.'/private/Studies/study.class.php');
include_once($path.'/private/Retailers/retailer.class.php');
include_once($path.'/private/Categories/category.class.php');
include_once($path.'/private/Platforms/platform.class.php');
include_once($path.'/private/Coments/coments.class.php');

