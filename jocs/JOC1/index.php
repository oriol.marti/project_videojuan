<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}
include_once($path.'/conf/conf.php');
session_start();

$usuario = new userService($conn);
if(!empty($_SESSION['user'])){
 $suser = unserialize($_SESSION['user']);
 $usuario->setuser($suser);  
}
$idUser = $usuario->getId();
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VideoJuan</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="../../style.css" >
    <link rel="stylesheet" href="./style2.css" >

</head>
<body class="min-vh-100">

<!-- partial:index.partial.html -->
<div id="score">Press UP</div>

<div id="controls">
  <div id="buttons">
    <button id="accelerate">
      <svg width="30" height="30" viewBox="0 0 10 10">
        <g transform="rotate(0, 5,5)">
          <path d="M5,4 L7,6 L3,6 L5,4" />
        </g>
      </svg>
    </button>
    <button id="decelerate">
      <svg width="30" height="30" viewBox="0 0 10 10">
        <g transform="rotate(180, 5,5)">
          <path d="M5,4 L7,6 L3,6 L5,4" />
        </g>
      </svg>
    </button>
  </div>
  <div id="instructions">
    Presiona la fletxa amunt per començar. Evita les col·lisions amb els altres vehicles
    accelerant o frenan amb les fletxes d'amunt i avall.
  </div>
</div>

<div id="results">
  <div class="content">
    <h1>You hit another vehicle</h1>
    <p>To reset the game press R</p>

<div>
  <form action="../../private/Videogames/operacions.php">
    <input type="hidden" name="operacio" value="puntuacio">
    <input type='hidden' name='idUser' value="<?=$usuario->getId()?>">
    <button class="btn btn-danger" type="submit" <?php if(!$idUser) echo 'disabled';?> >Guarda la puntuació</button>
  </form>
</div>

    <a id="result-youtube" target="_blank" style="display: none;" href="https://youtu.be/JhgBwJn1bQw">
      <div class="youtube" style="display: none;" ></div>
      <span style="display: none;" >Learn how to build this game with Three.js on YouTube</span>
    </a>

    <p>
      <a href="https://twitter.com/HunorBorbely" , target="_blank" style="display: none;" >@HunorBorbely</a>
    </p>
  </div>
</div>

<a id="youtube-main" class="youtube" style="display: none;"  target="_blank" href="https://youtu.be/JhgBwJn1bQw">
  <span>Learn Three.js</span>
</a>
<div id="youtube-card" style="display: hidden;" >
  Learn Three.js while building this game on YouTube
</div>
<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/three.js/r126/three.min.js'></script><script  src="./script.js"></script>
<script>

window.addEventListener("keydown", function(e) {
    if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
        e.preventDefault();
    }
}, false);

</script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
</body>
</html>