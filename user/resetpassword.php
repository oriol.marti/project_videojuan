<!DOCTYPE html>
<?php 
 ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); 
if(!isset($_SESSION)) {session_start();};
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
?>
<html>
<?php
echo '<head>
<link rel="stylesheet" href="aform.css">
</head>';
?>
<?php
    require '../header.php'
?>
<section>
<div class="login-dark">
    <!-- -------------------------------------------------------------------------

    http://localhost:8080/user/resetpassword.php?mail=oriol.marti.7e5@itb.cat

    -------------------------------------------------------------------------- -->
<form method="post" id="passres" action="controlador_users.php">
<small id="errors" style="color: red;padding: 3px;border:1px solid red;display:none;"></small><br><br>
    <h3 class="" style="text-align:center;">Password reset</h3>
    <div class="illustration"><i class="icon ion-ios-color-filter"></i></div>
        <div class="form-field">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" 
                    placeholder="Enter a strong password">
                <small></small>
        </div>

        <div class="form-field">
                <label for="username">Confirma Password:</label>
                <input type="password" name="confirm-password" id="confirm-password" 
                    placeholder="Reenter your password">
                <small></small>
        </div>
    <input type='hidden' name='usermail' value="<?php echo $_REQUEST['mail'];?>">
    <input type='hidden' name='operation' value="repassword">
             
    <div class="form-group"><button class="btn btn-primary btn-block" type="submit" name="verify_email" value="Verify Email">Cambia la password</button></div>
</form>
</div>
</section>
<?php
    /* require 'controlador_users.php' */
?>

<?php
    require '../footer.html'
?>
<?php
    require '../scripts.html'
?>
<script>
    const passwordEl = document.querySelector('#password');
    const confirmPasswordEl = document.querySelector('#confirm-password');

    const form = document.querySelector('#passres');

    const checkPassword = () => {

    let valid = false;

    const password = passwordEl.value.trim();

    if (!isRequired(password)) {
        showError(passwordEl, 'El password no pot estar buit');
    } else if (!isPasswordSecure(password)) {
        showError(passwordEl, 'El password ha de tenir 4 caracters, almenys 1 minuscula , 1 majuscula i 1 numero');
    } else {
        showSuccess(passwordEl);
        valid = true;
    }

    return valid;
    };

    const checkConfirmPassword = () => {
    let valid = false;
    // check confirm password
    const confirmPassword = confirmPasswordEl.value.trim();
    const password = passwordEl.value.trim();

    if (!isRequired(confirmPassword)) {
        showError(confirmPasswordEl, 'Torna a introduir el password');
    } else if (password !== confirmPassword) {
        showError(confirmPasswordEl, 'Les contrasenyes no son iguals');
    } else {
        showSuccess(confirmPasswordEl);
        valid = true;
    }

    return valid;
    };

    const isPasswordSecure = (password) => {
    const re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{4,})");
    return re.test(password);
};

    const isRequired = value => value === '' ? false : true;

    const showError = (input, message) => {
    // get the form-field element
    const formField = input.parentElement;
    // add the error class
    formField.classList.remove('success');
    formField.classList.add('error');

    // show the error message
    const error = formField.querySelector('small');
    error.textContent = message;
    };

const showSuccess = (input) => {
    // get the form-field element
    const formField = input.parentElement;

    // remove the error class
    formField.classList.remove('error');
    formField.classList.add('success');

    // hide the error message
    const error = formField.querySelector('small');
    error.textContent = '';
}

form.addEventListener('submit', function (e) {
    // prevent the form from submitting
    e.preventDefault();


    // validate forms
    let 
        isPasswordValid = checkPassword(),
        isConfirmPasswordValid = checkConfirmPassword();

    let isFormValid = isPasswordValid &&
        isConfirmPasswordValid;

    // submit to the server if the form is valid
    if (isFormValid) {
        form.submit();
    }
});


const debounce = (fn, delay = 500) => {
    let timeoutId;
    return (...args) => {
        // cancel the previous timer
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        // setup a new timer
        timeoutId = setTimeout(() => {
            fn.apply(null, args)
        }, delay);
    };
};
// 
form.addEventListener('input', debounce(function (e) {
    switch (e.target.id) {
        case 'password':
            checkPassword();
            break;
        case 'confirm-password':
            checkConfirmPassword();
            break;
    }
}));

</script>
</body>