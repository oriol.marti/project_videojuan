<!DOCTYPE html>
<?php 
 ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); 
if(!isset($_SESSION)) {session_start();};
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
?>
<html>
<?php
echo '<head>
<link rel="stylesheet" href="aform.css">
</head>';
?>
<?php
    require '../header.php'
?>
<section>
<div class="login-dark">
<form method="post" action="controlador_users.php">
<small id="errors" style="color: red;padding: 3px;border:1px solid red;display:none;"></small><br><br>
    <h3 class="" style="text-align:center;">Verifica</h3>
    <div class="illustration"><i class="icon ion-ios-checkmark-outline"></i></div>
    <input type='hidden' name='operation' value="verify">
    <input type="hidden" name="email" value="<?php $_REQUEST['email']; ?>" required>
    <div class="form-group"><input class="form-control" type="text" name="verification_code" placeholder="Enter verification code" required ></div>
    <div class="form-group"><button class="btn btn-primary btn-block" type="submit" name="verify_email" value="Verify Email">Verifica el teu mail</button></div>
</form>
</div>
</section>
<?php
    /* require 'controlador_users.php' */
?>

<?php
    require '../footer.html'
?>
<?php
    require '../scripts.html'
?>
</body>