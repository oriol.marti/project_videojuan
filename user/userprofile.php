<!DOCTYPE html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!isset($_SESSION)) {
    session_start();
};
?>
<html>
<?php
require '../header.php';
if (isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])) {
    $path = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
} else {
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path . '/conf/conf.php');
$myVideogame = new Videogame($conn);
?>
<!-- ADD CONTENT HERE -->

<body>


    <div class="container emp-profile">
        <h1 style="text-align:center;"> Perfil de <?= $usuario->getusername() ?> </h1>
        <a href="edit_profile.php" class="float-end mr-5 mt-5">EDITAR PERFIL </a>
        <div class="row">
            <div class="col-md-4">
                <div class="profile-img">

                    <form method="post" action="controlador_users.php" id="formavatar" enctype="multipart/form-data">
                        <small id="errors" style="color: red;padding: 3px;border:1px solid red;display:none;"></small><br><br>
                        <input type='hidden' name='operation' value="uploadavatar">
                        <input type='hidden' name='id' value="<?= $usuario->getId() ?>">
                        <img src="/assets/users_avatar/<?= $usuario->getavatar() ?>" alt="" />
                        <label class="form-label mt-2">
                            Cambia foto de perfil:
                        </label>
            
                            <input type="file" class="form-control mb-3" name="file" id="avatar" height="200" width="15" />

                        <button type="submit" class="buttons third" id="btn-avatar" style="margin: auto;">CAMBIAR FOTO</button>
                    </form>
                </div>
            </div>
            <br>
            <div class="col-md-6">
                <div class="profile-head text-white mt-5">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Informació d'Usuari</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Els teus Scores</a>
                        </li>
                    </ul>
                    <div class="row">

                        <div class="col-md-8 mt-0">
                            <div class="tab-content profile-tab" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>ID de Usuari</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p><?= $usuario->getId() ?></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Nom de usuari</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p><?= $usuario->getname() ?></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Cognom de usuari</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p><?= $usuario->getsurname() ?></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Email de Usuari</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p><?= $usuario->getmail() ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Cars Score:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p><?= $usuario->getscore1() ?></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Flappy Birds Score:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p><?= $usuario->getscore2() ?></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>SkyBlock Score:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p><?= $usuario->getscore3() ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h1 class="text-center mt-5"> LES TEVES CAPTURES</h1>
        <div class="masonry">
            <?php
            $videogameUser = new Videogame($conn);
            $screenshotlis = $videogameUser->llistaScreenshot("iduser =" . $usuario->getId());
            foreach ($screenshotlis as $s) { ?>
                <div class="mItem">
                    <img class="img_m" src="../private/uploads/<?= $s['screenshotUrl'] ?>">
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php
    require '../footer.html';
    require '../scripts.html';
    ?>
</body>

</html>