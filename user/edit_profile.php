<!DOCTYPE html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!isset($_SESSION)) {
	session_start();
};

echo '<head>
<link rel="stylesheet" href="aform.css">
</head>';
?>

<html>
<?php
require '../header.php';
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
$myVideogame = new Videogame($conn);
?>
<!-- ADD CONTENT HERE -->
<body>
    <div class="container emp-profile ">
    <small id="errors" style="color: red;padding: 3px;border:1px solid red;display:none;"></small><br><br>
    <h2 class="mb-5 ml-5">EDITAR USUARI:</h2>
        <div class="row align-items-start">
            <div class="col-5">
            
                        <div class="profile-img ">
                        
                            <img  src="/assets/users_avatar/<?=$usuario->getavatar()?>" class="rounded-circle" alt=""/>
                        </div> 
            </div>
            <div class="col-2">
            <h5>
                                    <?= $usuario->getusername() ?>
                                    </h5>
                                    <h6 style="margin-bottom: 30%;color: blue;">
                                        <?= $usuario->getrights() ?>
                                    </h6>
            </div>
            <div class="col">
            <form method="post" id="editform" action="controlador_users.php">
                
                <input type='hidden' id='operation' name='operation' value="profile_edit">
                <input type='hidden' name='id' value="<?=$usuario->getId()?>">
                <div class="row align-items-start">
                    
                                    
                                
                       
                        
                    <div class="col col-md-10">
                        
                        <div class="tab-content profile-tab"  id="myTabContent">
                        
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="row">
                                        <div class="form-field">
                                            <label for="username">Username:</label>
                                            <input type="text" name="username" id="username" autocomplete="off" placeholder="Cambia username" value="<?=$usuario->getusername()?>">
                                            <small></small>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="form-field">
                                            <label for="Email">Email:</label>
                                            <input type="text" name="email" id="email" autocomplete="off" placeholder="Cambia email" value="<?=$usuario->getmail()?>">
                                            <small></small>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="form-field">
                                            <label for="Email">Nom:</label>
                                            <input type="text" name="nom" id="nom" autocomplete="off" placeholder="Cambia nom" value="<?=$usuario->getsurname()?>">
                                            <small></small>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="form-field">
                                            <label for="Email">Cognom:</label>
                                            <input type="text" name="cognom" id="cognom" autocomplete="off" placeholder="Cambia cognom" value="<?=$usuario->getsurname()?>">
                                            <small></small>
                                        </div>
                                        <button type="submit" class="profile-edit-btn ml-3 mt-4" id="btn-avatar" >GUARDAR</button>
                                        </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </form>           
            </div>
        </div>
        
           
        </div>
	<?php
		require '../footer.html';
		require '../scripts.html';
	?>
	</body>
    <script>

const usernameEl = document.querySelector('#username');
const emailEl = document.querySelector('#email');
const form = document.querySelector('#editform');


const checkUsername = () => {

    let valid = false;

    const min = 3,
        max = 25;

    const username = usernameEl.value.trim();

    if (!isRequired(username)) {
        showError(usernameEl, 'El username no pot estar buit');
    } else if (!isBetween(username.length, min, max)) {
        showError(usernameEl, `El username ha de tenir entre ${min} i ${max} caracters.`)
    } else {
        showSuccess(usernameEl);
        valid = true;
    }
    return valid;
};

const checkEmail = () => {
    let valid = false;
    const email = emailEl.value.trim();
    if (!isRequired(email)) {
        showError(emailEl, 'El email no pot estar buit.');
    } else if (!isEmailValid(email)) {
        showError(emailEl, 'El email no es valit')
    } else {
        showSuccess(emailEl);
        valid = true;
    }
    return valid;
};


const isEmailValid = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

const isRequired = value => value === '' ? false : true;
const isBetween = (length, min, max) => length < min || length > max ? false : true;

/* const isValitName = (name) => name ? true : false;
const isValitEmail = (email) => email ? true : false; */

const showError = (input, message) => {
    // get the form-field element
    const formField = input.parentElement;
    // add the error class
    formField.classList.remove('success');
    formField.classList.add('error');

    // show the error message
    const error = formField.querySelector('small');
    error.textContent = message;
};

const showSuccess = (input) => {
    // get the form-field element
    const formField = input.parentElement;

    // remove the error class
    formField.classList.remove('error');
    formField.classList.add('success');

    // hide the error message
    const error = formField.querySelector('small');
    error.textContent = '';
}


form.addEventListener('submit', function (e) {
    // prevent the form from submitting
    e.preventDefault();


    // validate forms
    let isUsernameValid = checkUsername(),
        isEmailValid = checkEmail();

    let isFormValid = isUsernameValid &&
        isEmailValid;

    // submit to the server if the form is valid
    if (isFormValid) {
        /* form.action = "/controlador_users.php" */;
        form.submit();
    }
});


const debounce = (fn, delay = 500) => {
    let timeoutId;
    return (...args) => {
        // cancel the previous timer
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        // setup a new timer
        timeoutId = setTimeout(() => {
            fn.apply(null, args)
        }, delay);
    };
};
// 
form.addEventListener('input', debounce(function (e) {
    switch (e.target.id) {
        case 'username':
            checkUsername();
            break;
        case 'email':
            checkEmail();
            break;
    }
}));
    </script>

</html>