<?php

class userService {

    protected $conn = NULL; 
    protected $user = NULL; //guardem la informacio del usuari
    public $user_loggin = false; //fem un check per a si el usuari esta loggeat

    function __construct($conn){   
        $this->conn = $conn;
    }

    function getId(){
        return $this->user['iduser'];
    }
    function getname(){
        return $this->user['name_user'];
    }
    function getpass(){
        return $this->user['userpass'];
    }
    function getsurname(){
        return $this->user['user_surname'];
    }
    function getmail(){
        return $this->user['useremail'];
    }
    function getusername(){
        return $this->user['username'];
    }
    function getrights(){
        return $this->user['user_rights'];
    }
    function getavatar(){
        if (!EMPTY($this->user['profile_avatar'])){
            return $this->user['profile_avatar'];
        }else{
            return "default-profile.png";
        }
    }
    function setuser($usersession){
        $this->user = $usersession;
    }
    function getverfcode(){
        return $this->user['verification_code'];
    }
    function setavatar($url){
        $this->user['profile_avatar'] = $url;
    }
    function getscore1(){
        return $this->user['puntuacioJoc1'];
    }
    function getscore2(){
        return $this->user['puntuacioJoc2'];
    }
    function getscore3(){
        return $this->user['puntuacioJoc3'];
    }
//funciona
    function userlogin($user_name, $password_encripted){
        if($this->user_loggin === true){
            //check session;
            if(!$_SESSION['iduser'] === $this->user['iduser']){
                $this->user_loggin = false;
                return false;
            }else {
                return true;
            }

        }else if($this->user_loggin === false)
            /* $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); */
            echo "SELECT * FROM `users` WHERE `username` = '$user_name' && `userpass` = '$password_encripted'";
            $laMevaSentencia = $this->conn->prepare("SELECT * FROM `users` WHERE `username` = '$user_name' && `userpass` = '$password_encripted'"); 

            $laMevaSentencia->setFetchMode(PDO::FETCH_ASSOC);
            $laMevaSentencia->execute();
            $count = $laMevaSentencia->rowCount();

            if($count > 0){
            $this->user = $laMevaSentencia->fetch(); 
                $_SESSION['user'] = serialize($this->user); 
                $_SESSION['iduser'] = serialize($this->user['iduser']); 
                $_SESSION['username'] = serialize($this->user['username']); 
                $_SESSION['userpass'] = serialize($this->user['userpass']); 
                $_SESSION['verification_code'] = serialize($this->user['verification_code']); 
                $_SESSION['user_validation'] = serialize($this->user['verify_account']); 
                $session_lifetime = 3600 * 24 * 2; // 2 days
                session_set_cookie_params ($session_lifetime);
                $this->user_loggin = true;
                return true;
            }else {
                return false;
            }
    }
    
    function logout(){
        // set variables to null
        session_destroy();
        $this->user_loggin = false;

    }
//funciona
    function userregistration($user_name, $encrypted_password, $email, $verification_code){
        /* $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); */
        $stmt = $this->conn ->prepare("INSERT INTO `users` ( `username`, `userpass`, `useremail`, `verification_code`) VALUES ( ?, ?, ?, ?)");
        $stmt->execute(array($user_name, $encrypted_password, $email, $verification_code));
    }

    function uservalidation($code){
            /* $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); */
            $laMevaSentencia = $this->conn->prepare("SELECT * FROM `users` WHERE `iduser` = ?");
            $laMevaSentencia->setFetchMode(PDO::FETCH_ASSOC);
            $laMevaSentencia->execute(array(unserialize($_SESSION['iduser'])));
            $count = $laMevaSentencia->rowCount();
            echo unserialize($_SESSION['iduser']);
            var_dump($_SESSION);
            echo "count: ".$count;
        if($count > 0){ 
            $user = $laMevaSentencia->fetch(); 
            if($user['verification_code'] == $code){
                    echo "dintre del if";
                    /* $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); */
                    $stmt = $this->conn ->prepare("UPDATE `users` SET `verify_account` = ? WHERE `users`.`iduser` = ?;");
                    $stmt->execute(array(date("Y-m-d") , unserialize($_SESSION['iduser'])));
                    
                    return true;
            }else{echo "else"; return false;}//error del codigo de verificacion
        }else{
            echo "ERROR "; return false;
        }
    }

    function llistar($filter=0,$order=0){
        if(!$filter)$filter = "";
        else $filter = " WHERE ".$filter;
        if(!$order)$order = "";
        else $order = " ORDER BY ".$order;
         $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
        $laMevaSentencia = $this->conn->prepare("SELECT * FROM `users` $filter $order");
		$laMevaSentencia->execute();
        $arrayusers = $laMevaSentencia->fetchAll();
        return($arrayusers);
    }

    function useredit($iduser,$newusername,$newname, $newsurname, $newmail){
        /* $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); */
        $stmt = $this->conn ->prepare("UPDATE `users` SET `username` = ?,`name_user` = ?, `user_surname` = ?,`useremail` = ? WHERE `users`.`iduser` = ?;");
        $stmt->execute(array($newusername, $newname, $newsurname,$newmail,$iduser));
        // $usuario->llistar("`iduser` = ".$usuario->getId())
    }

    function passedit($oldpass, $newpass){
        /* $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); */
        /* if(getpass() = $oldpass ){} */
        $stmt = $this->conn ->prepare("UPDATE `users` SET `userpass` = ? WHERE `users`.`iduser` = ?;");
        $stmt->execute(array($newpass));
        
    }

    function avatar($fileNameTmp, $userid){
        
        $statusMsg = '';

        $targetDir = "../assets/users_avatar/";
        $fileName = $fileNameTmp;
        $targetFilePath = $targetDir . $fileName;
        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

        if(!empty($_FILES["file"]["name"])){
            // Allow certain file formats
            $allowTypes = array('jpg','png','jpeg','gif','pdf');
            if(in_array($fileType, $allowTypes)){
                echo "\n <br> ".$fileName;
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    echo "Filename: ".$fileName;
                    echo "\n <br> ENTRO 2##";
                    // Insert image file name into database
                    $insert = $this->conn->prepare("UPDATE `users` SET `profile_avatar` = ? WHERE `users`.`iduser` = ?;");
                    $insert->execute(array($fileName,$userid));
                    if($insert){
                        echo "\n <br> ENTRO 3##";
                        $statusMsg = "The file ".$fileName. " has been uploaded successfully.";
                        return true;
                    }else{
                        $_SESSION['ERROR'] = serialize( "File upload failed, please try again.");
                        echo $_SESSION['ERROR'];
                        return false;
                    }
                }else{
                    $_SESSION['ERROR'] = serialize( "ERROR al pujar aquest avatar.");
                    echo $_SESSION['ERROR'];
                    return false;
                }
            }else{
                $_SESSION['ERROR'] = serialize( 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.');
                echo $_SESSION['ERROR'];
                return false;
            }
        }else{
            $_SESSION['ERROR'] = serialize( 'Selecciona una imatge');
            echo $_SESSION['ERROR'];
            return false;
        }

        // Display status message
        echo $statusMsg;
        return true;

    }
    function resetpass($newpass, $mail){
        $stmt = $this->conn ->prepare("UPDATE `users` SET `userpass` = ? WHERE `users`.`useremail` = ?;");
        //$stmt->execute(array($newpass,$mail));
        return $stmt->execute(array($newpass,$mail));
    }
    
}
