<!DOCTYPE html>
<?php 
 ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); 
if(!isset($_SESSION)) {session_start();};
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
?>
<html>
<?php
echo '<head>
<link rel="stylesheet" href="aform.css">
</head>';
?>
<?php
    require '../header.php'
?>
<section>
<div class="login-dark">
<form method="post" id="passres" action="controlador_users.php">
<small id="errors" style="color: red;padding: 3px;border:1px solid red;display:none;"></small><br><br>
    <h3 class="" style="text-align:center;">Recuperació</h3>
    <div class="illustration"><i class="icon ion-ios-email-outline"></i></div>
    <input type='hidden' name='operation' value="recemail">
        <div class="form-field">
                <label class="text-white" for="Email">Email de la conta que vols recuperar:</label>
                <input type="text" name="email" id="email" autocomplete="off" placeholder="Enter your email">
                <small></small>
            </div>
    <div class="form-group"><button class="btn btn-primary btn-block" type="submit" name="verify_email" value="Verify Email">Enviar Mail</button></div>
    <br>
</form>
</div>
</section>
<?php
    /* require 'controlador_users.php' */
?>

<?php
    require '../footer.html'
?>
<?php
    require '../scripts.html'
?>
<script>
    const emailEl = document.querySelector('#email');

    const form = document.querySelector('#passres');

    const checkEmail = () => {
    let valid = false;
    const email = emailEl.value.trim();
    if (!isRequired(email)) {
        showError(emailEl, 'El email no pot estar buit.');
    } else if (!isEmailValid(email)) {
        showError(emailEl, 'El email no es valit')
    } else {
        showSuccess(emailEl);
        valid = true;
    }
  

    return valid;
    };

    const isEmailValid = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
    };

    const isRequired = value => value === '' ? false : true;

    const showError = (input, message) => {
    // get the form-field element
    const formField = input.parentElement;
    // add the error class
    formField.classList.remove('success');
    formField.classList.add('error');

    // show the error message
    const error = formField.querySelector('small');
    error.textContent = message;
    };

const showSuccess = (input) => {
    // get the form-field element
    const formField = input.parentElement;

    // remove the error class
    formField.classList.remove('error');
    formField.classList.add('success');

    // hide the error message
    const error = formField.querySelector('small');
    error.textContent = '';
}

form.addEventListener('submit', function (e) {
    // prevent the form from submitting
    e.preventDefault();


    // validate forms
    let 
        isEmailValid = checkEmail();

    let isFormValid = isEmailValid;

    // submit to the server if the form is valid
    if (isFormValid) {
        form.submit();
    }
});


const debounce = (fn, delay = 500) => {
    let timeoutId;
    return (...args) => {
        // cancel the previous timer
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        // setup a new timer
        timeoutId = setTimeout(() => {
            fn.apply(null, args)
        }, delay);
    };
};
// 
form.addEventListener('input', debounce(function (e) {
    switch (e.target.id) {
        case 'email':
            checkEmail();
            break;
    }
}));

</script>
</body>