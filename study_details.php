<!DOCTYPE html>
<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

if (!isset($_SESSION)) {
	session_start();
};
?>
<html>

<head>
	<link rel="stylesheet" href="/study_details.css">
</head>

<?php
require './header.php';
if (isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])) {
	$path = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
} else {
	$path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path . '/conf/conf.php');
$myStudy = new Study($conn);
$myComent = new Coment($conn);
$idUser = $usuario->getId();

?>

<!-- ADD CONTENT HERE -->

<body>
	<div class="container-fluid p-5">
		<section>
			<article class="container">
				<div class="div_container">
					<?php

					$idStudyTMP = 0;
					$a_myStudy = [];
					if ($a_myStudy = $myStudy->llista()) {
						foreach ($a_myStudy as $myStudyTMP) {
							if ($myStudyTMP['idStudy'] == $_GET['value']) {
								$idStudyTMP = $myStudyTMP['idStudy'];
								if (!$idUser) $idUser = 0;
								$a_votes = $myStudy->llistaRel('votes', 'idStudy= ' . $idStudyTMP);
								$idVotedUser = [];
								foreach ($a_votes as $votes) {
									array_push($idVotedUser, $votes['idUser']);
								}
								$voted = false;
								if (in_array($idUser, $idVotedUser)) $voted = true;
					?>
								<img class="img" src="<?= $myStudyTMP['studyLogo'] ?>">
								<div>
									<h3><?= $myStudyTMP['studyName'] ?></h3>
									<h4><?= $myStudyTMP['studyYear'] ?></h4>
									<p class="synop"><?= $myStudyTMP['studyBio'] ?></p>
									<div style="width: 20%;">
										<form class="form" action="private/Studies/studiesOperacions.php">
											<input type='hidden' name='idStudy' value="<?= $myStudyTMP['idStudy'] ?>">
											<input type='hidden' name='idUser' value="<?= $idUser ?>">
											<input type='hidden' name='public' value="public">
											<button class="btn buttons third" type='submit' name='operacio' value="Like" <?php if (!$idUser) {
																																echo 'disabled';
																															} else if ($voted) {
																																echo 'disabled';
																															} else {
																																echo '';
																															}; ?>><span class="iconify" data-icon="ion:thumbs-up" data-width="25"></span></button>
											<button class="btn buttons third ms-2" type='submit' name='operacio' value="Dislike" <?php if (!$idUser) {
																																		echo 'disabled';
																																	} else if ($voted) {
																																		echo 'disabled';
																																	} else {
																																		echo '';
																																	}; ?>><span class="iconify" data-icon="ion:thumbs-down" data-width="25"></span></button>
										</form>
									</div>
									<div class="div_likes">
										<p>Likes: <?= $myStudyTMP['studyLikes'] ?></p>
										<p class="pl-2">Dislikes: <?= $myStudyTMP['studyDislikes'] ?></p>
									</div>
								</div>
				</div>
	<?php
							}
						}
					}
	?>
			</article>
		</section>
		<section class="section2">
			<article>
				<div class="container-fluid">
					<h4>Videojocs del Estudi:</h4>
					<table class="table table-hover table-striped">
						<tbody>
							<?php
							$myVideogame = new Videogame($conn);
							$a_myVideogame = [];
							if ($a_myVideogame = $myVideogame->llista('idStudy=' . $idStudyTMP)) {
								foreach ($a_myVideogame as $myVideogameTMP) {
							?>
									<tr>
										<td class="col-2"><a href="/game_details.php?value=<?= $myVideogameTMP['idVideogame'] ?>"><img class="list_img" src="<?= $myVideogameTMP['videogameLogo'] ?>" alt=""></a></td>
										<td class="col-6"><a href="/game_details.php?value=<?= $myVideogameTMP['idVideogame'] ?>"><?= $myVideogameTMP['videogameName'] ?></a></td>
									</tr>
							<?php
								}
							}
							?>
						</tbody>
					</table>
				</div>
			</article>
		</section>
		<section class="section2">
			<div class="container-fluid">
				<?php
				$userTMP = new userService($conn);
				$a_myComents = [];
				if ($a_myComents = $myComent->llista('idStudy= ' . $idStudyTMP, 'comentLikes')) {
					foreach ($a_myComents as $myComentTMP) {
						if ($a_users = $userTMP->llistar('iduser= ' . $myComentTMP['idUser'])) {
							foreach ($a_users as $userNames) {
								$idUserTMP = $userNames['iduser'];
								$userName = $userNames['username'];
							}
						};
						$a_votesComents = $myComent->llistaRel('idComent= ' . $myComentTMP['idComent']);
						foreach ($a_votesComents as $votes) {
							$idVotedUser = $votes['idUser'];
						}
						$a_votesComents = $myComent->llistaRel('idComent= ' . $myComentTMP['idComent']);
						$idVotedUserComent = [];
						foreach ($a_votesComents as $votes) {
							array_push($idVotedUserComent, $votes['idUser']);
						}
						$votedComent = false;
						if (in_array($idUser, $idVotedUserComent)) $votedComent = true;
				?>
						<div class="comentari col-12 p-2">
							<div class="col-md-9">
								<p>Comentari: <?= $myComentTMP['comentTxt'] ?></p>
								<p>Usuari: <?= $userName ?></p>
							</div>
							<div class="opcionsComentaris col-md-3 d-flex justify-content-around">
								<div class="d-block">
									<form class="form pb-3" action="private/Coments/comentsOperacions.php">
										<input type='hidden' name='idComent' value="<?= $myComentTMP['idComent'] ?>">
										<input type='hidden' name='idStudy' value="<?= $idStudyTMP ?>">
										<input type='hidden' name='publicStudy' value="publicStudy">
										<button class="btn buttons third" type='submit' name='operacio' value="Like" <?php if (!$idUser) {
																															echo 'disabled';
																														} else if ($votedComent) {
																															echo 'disabled';
																														} else {
																															echo '';
																														}; ?>><span class="iconify" data-icon="ion:thumbs-up" data-width="25"></span></button>
										<button class="btn buttons third ml-1" type='submit' name='operacio' value="Dislike" <?php if (!$idUser) {
																																	echo 'disabled';
																																} else if ($votedComent) {
																																	echo 'disabled';
																																} else {
																																	echo '';
																																}; ?>><span class="iconify" data-icon="ion:thumbs-down" data-width="25"></span></button>
									</form>
									<div class="div_likes">
										<p>Likes: <?= $myComentTMP['comentLikes'] ?></p>
										<p class="pl-2">Dislikes: <?= $myComentTMP['comentDislikes'] ?></p>
									</div>
								</div>

								<form action='private/Coments/comentsOperacions.php'>
									<input type='hidden' name='idComent' value="<?= $myComentTMP['idComent'] ?>">
									<input type="hidden" name="idStudy" value="<?= $idStudyTMP ?>">
									<input type='hidden' name='operacio' value="elimina">
									<button class='btn buttons third ml-1' type='submit' <?php if (!$idUser || $idUser != $idUserTMP) echo 'disabled'; ?>><span class="iconify" data-icon="ion:trash" data-width="25"></span></button></button>
								</form>

							</div>
						</div>
				<?php
					}
				}
				?>
				<div class="comentTxt">
					<form action="private/Coments/comentsOperacions.php">
						<input type="hidden" name="operacio" value="insertar">
						<input type="hidden" name="idStudy" value="<?= $idStudyTMP ?>">
						<input type='hidden' name='publicStudy' value="publicStudy">
						<textarea name="comentTxt" id="comentTxt" rows="5" style="width: 100%;" <?php if (!$idUser) echo 'disabled'; ?> required></textarea>
						<div id="div_comentTxt">
							<button class='btn buttons third' type="submit" <?php if (!$idUser) echo 'disabled'; ?>>Comenta</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
	<?php
	require './footer.html';
	require './scripts.html';
	?>
</body>

</html>