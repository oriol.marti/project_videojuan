<?php 
if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}
include_once($path.'/conf/conf.php');

$usuario = new userService($conn);
if(!empty($_SESSION['user'])){
 $suser = unserialize($_SESSION['user']);
 $usuario->setuser($suser);  
}

if(!empty($_SESSION['ERROR'])){
    $error = unserialize($_SESSION['ERROR']);
}
?>
<script>
    window.addEventListener('load', function () {
    let error = "<?= $error; ?>";
    console.log(error);
    
    if(!error.length == 0){
    document.getElementById("errors").style.display="block";
    document.getElementById("errors").innerHTML = error;
    }else{
        <?php $_SESSION['ERROR']= serialize('');?>
    }
})
</script>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="/assets/images/videojuan_logo.png">
    <title>VideoJuan</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
     <?php if(file_exists("style.css")){echo '<link rel="stylesheet" href="style.css" >';}else{echo '<link rel="stylesheet" href="../style.css" >';}?>
     <?php if(file_exists("profilestyle.css")){echo '<link rel="stylesheet" href="./profilestyle.css" >';}?>
	<script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>

    
<!--
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-R10T0F37F0"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-R10T0F37F0');
    </script> -->
</head>
<body>
<header>
    <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search">
        <div class="container"><a class="navbar-brand" href="/index.php"><img src="/assets/images/videojuan_logo.png" height="30px">Videojuan</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-between" id="navcol-1">
                <ul class="nav navbar-nav">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="/videogamesLlista.php">Jocs</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="/studiesLlista.php">Estudis</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="/jocJsLlista.php">Videojocs JS</a></li>
                </ul>
                <div class="d-flex">
                <?php 
                if(!empty($_SESSION['username'])){ ?>
                        <img src="/assets/users_avatar/<?= $usuario->getavatar()?>" style="border-radius: 50%;
                        border: 1px solid rgba(0, 0, 0, 0.1); margin-right:2px" height="40px">

                        <span class="navbar-text username"> <a class="login" href="/user/userprofile.php"><?=$usuario->getusername()?></a></span>
                        <?php

                        if($usuario->getrights()=='admin'){
                        ?>
                        <span class="navbar-text"> <a href="/private/Videogames/videogames.php" class="login" target="blank">Administració</a></span>
                            <?php
                        }
                            ?>
                       
                        <form method="post" action="/user/controlador_users.php">
                            <input type="hidden" name="operation" value="logout">

                            <button type="submit" class="btn btn-light action-button button-logout">
                                <i class="icon icon-signout"></i>
                            </button>

                        </form> 
                <?php }else{  ?>
                        <span class="navbar-text"> <a href="/user/login.php" class="login">Log In</a></span>

                        <a class="btn btn-light action-button" role="button" href="/user/signin.php">Sign Up</a>
                <?php } ?>
                </div>
            </div>
        </div>
    </nav>
</header>

