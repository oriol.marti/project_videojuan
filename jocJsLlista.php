<?php 
//0) activo els errors
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();


if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require './header.php';
?>
<body>
	<h1 class="text-center mt-2">VIDEOJOCS JS</h1>
	<div class="container-fluid llista">
		<section>
            <table class="table table-hover table-striped">
                <tbody>
                    <tr>
                        <td class="col-2"><a href="/joc1.php" class="disabled"><img class="list-img" src="assets/images/Captura de pantalla de 2022-05-09 16-46-49.png" alt=""></a></td>
                        <td class="col-6 td_title"><a class="h5 text-white" href="/jocs/JOC1/index.php"></a><br><p class="pText">Cotxes</p><br>
                        <p class="pText">No disponible per movil!</p></td>
                        <td class="col-1"><p class="pLlista">Ranking:</p><p class="text-white movil">No disponible per movil!</p></td>
                        <td class="col-2"><p class="pLlista">
                            <?php
                            $myUser = new userService($conn);
                            $a_users = $myUser->llistar('puntuacioJoc1>0','puntuacioJoc1 DESC');
                            $cont = 0;
                            foreach($a_users as $users){
                                echo $users['username'] . '-> ';
                                echo $users['puntuacioJoc1'];
                                echo '<br>';
                                $cont++;
                                if($cont==5)break;
                            }
                            ?>
                        </p><br>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-2"><a href="/joc2.php"><img class="list-img" src="assets/images/flappy.png" alt=""></a></td>
                        <td class="col-6 td_title"><a class="h5 text-white" href="/jocs/JOC1/index.php"></a><br><p class="pText">Flappy Bird</p></td>
                        <td class="col-1"><p class="pLlista">Ranking:</p></td>
                        <td class="col-2"><p class="pLlista">
                            <?php
                            $myUser = new userService($conn);
                            $a_users = $myUser->llistar('puntuacioJoc2>0','puntuacioJoc2 DESC');
                            $cont = 0;
                            foreach($a_users as $users){
                                echo $users['username'] . '-> ';
                                echo $users['puntuacioJoc1'];
                                echo '<br>';
                                $cont++;
                                if($cont==5)break;
                            }
                            ?>
                        </p><br>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-2"><a href="/joc3.php" class="disabled"><img class="list-img" src="assets/images/tower.png" alt=""></a></td>
                        <td class="col-6 td_title"><a class="h5 text-white" href="/jocs/JOC1/index.php"></a><br><p class="pText">Tower</p><br>
                        <p class="pText">No disponible per movil!</p></td>
                        <td class="col-1"><p class="pLlista">Ranking:</p><p class="text-white movil">No disponible per movil!</p></td>
                        <td class="col-2"><p class="pLlista">
                            <?php
                            $myUser = new userService($conn);
                            $a_users = $myUser->llistar('puntuacioJoc3>0','puntuacioJoc3 DESC');
                            $cont = 0;
                            foreach($a_users as $users){
                                echo $users['username'] . '-> ';
                                echo $users['puntuacioJoc1'];
                                echo '<br>';
                                $cont++;
                                if($cont==5)break;
                            }
                            ?>
                        </p><br>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h1 class="text-center">Mes videojocs proximament...</h1>
	    </section>
	</div>
    <?php
	require './footer.html';
	require './scripts.html';
	?>
</body>

</html>