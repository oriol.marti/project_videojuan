<!DOCTYPE html>
<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

if (!isset($_SESSION)) {
	session_start();
};
?>
<html>

<?php
require './header.php';
if (isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])) {
	$path = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
} else {
	$path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path . '/conf/conf.php');
$myVideogame = new Videogame($conn);
$myComent = new Coment($conn);
$myStudy = new Study($conn);
$idUser = $usuario->getId();

?>

<head>
	<link rel="stylesheet" href="/game_details.css">
</head>
<!-- ADD CONTENT HERE -->
<?php

$idVideogameTMP = 0;
$a_myVideogame = [];
if ($a_myVideogame = $myVideogame->llista()) {
	foreach ($a_myVideogame as $myVideogameTMP) {
		if ($myVideogameTMP['idVideogame'] == $_GET['value']) {
			$idVideogameTMP = $myVideogameTMP['idVideogame'];
			if (!$idUser) $idUser = 0;
			$a_votes = $myVideogame->llistaRel('votes', 'idVideogame= ' . $idVideogameTMP);
			$idVotedUser = [];
			foreach ($a_votes as $votes) {
				array_push($idVotedUser, $votes['idUser']);
			}
			$voted = false;
			if (in_array($idUser, $idVotedUser)) $voted = true;
?>

			<body>
				<div class="container-fluid p-5">
					<h1><?= $myVideogameTMP['videogameName'] ?></h1>
					<section class="section">
						<article class="cont">
							<div class="div2">
								<div class="div_principal">
									<img class="img" src="<?= $myVideogameTMP['videogameLogo'] ?>">
								</div>
								<div>
									<b>
										<p class="synop text-center"><?= $myVideogameTMP['videogameSynopsis'] ?></p>
									</b>
									<b>
										<p class="texto text-center"><?= $myVideogameTMP['videogame_text'] ?></p>
									</b>
								</div>
							</div>
							<div class="div1">
								<?php
								$myVideos = new Video($conn);
								$videosVideogame = [];
								if ($videosVideogame = $myVideos->llista()) { 
									foreach ($videosVideogame as $myVideo) {
										if ($myVideo['idVideogame'] == $_GET['value']) {
								?>
											<div id="carouselExampleControls" class="carousel w-100" data-bs-ride="carousel">
												<div class="carousel-inner">
													<div class="carousel-item active">
														<iframe src="<?= $myVideo['videoUrl'] ?>" class="d-block w-100" height="500" frameborder="0"></iframe>
													</div>
													<?php
													$screenshots = [];
													if ($screenshots = $myVideogame->llistaScreenshot('idUser=0')) {
														foreach ($screenshots as $myScreens) {
															if ($myScreens['idVideogame'] == $_GET['value']) {
													?>
																<div class="carousel-item">
																	<img src="/private/uploads/<?= $myScreens['screenshotUrl'] ?>" height="500" class="d-block w-100">
																</div>
													<?php
															}
														}
													}
													?>
												</div>
												<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
													<span class="carousel-control-prev-icon" aria-hidden="true"></span>
													<span class="visually-hidden">Previous</span>
												</button>
												<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
													<span class="carousel-control-next-icon" aria-hidden="true"></span>
													<span class="visually-hidden">Next</span>
												</button>
											</div>
											<?php
											$a_myStudy = [];
											$limit = 1;
											if ($a_myStudy = $myStudy->llista('idStudy=' . $myVideogameTMP['idStudy'])) {
												foreach ($a_myStudy as $myStudyTMP) {
											?>
													<div class="div_container">
														<div>
															<h5>DATA LLENÇAMENT:<?= ' ' . $myVideogameTMP['videogameYear'] ?></h5>
															<h5>ESTUDI:<?= ' ' . $myStudyTMP['studyName'] ?></h5>
													<?php
												}
											}
											$cont = 5;
											for ($i = 0; $i < $myVideogameTMP['videogameRating']; $i++) {
												echo "★";
												$cont--;
											}
											for ($i = 0; $i < $cont; $i++) {
												echo "☆";
											}
													?>
													<form class="form pb-3 mt-3" action="private/Videogames/operacions.php">
														<input type='hidden' name='idVideogame' value="<?= $myVideogameTMP['idVideogame'] ?>">
														<input type='hidden' name='idUser' value="<?= $idUser ?>">
														<input type='hidden' name='public' value="public">
														<button class="btn buttons third" type='submit' name='operacio' value="Like" <?php if (!$idUser) {
																																			echo 'disabled';
																																		} else if ($voted) {
																																			echo 'disabled';
																																		} else {
																																			echo '';
																																		}; ?>><span class="iconify" data-icon="ion:thumbs-up" data-width="25"></span></button>
														<button class="btn buttons third ml-2" type='submit' name='operacio' value="Dislike" <?php if (!$idUser) {
																																					echo 'disabled';
																																				} else if ($voted) {
																																					echo 'disabled';
																																				} else {
																																					echo '';
																																				}; ?>><span class="iconify" data-icon="ion:thumbs-down" data-width="25"></span></button>
													</form>
													<div class="div_likes">
														<p>Likes: <?= $myVideogameTMP['videogameLikes'] ?></p>
														<p class="pl-2">Dislikes: <?= $myVideogameTMP['videogameDislikes'] ?></p>
													</div>
													<form action="private/Videogames/operacions.php" method="post" enctype="multipart/form-data">
														<label class="form-label">
															Selecciona una imatge per pujar:
														</label>
														<input type='hidden' name='idVideogame' value="<?= $myVideogameTMP['idVideogame'] ?>">
														<input type='hidden' name='idUser' value="<?= $idUser ?>">
														<input type='hidden' name='public' value="public">
														<input type="hidden" name="operacio" value="screenshot">
														<input class="inputImg form-control mb-3" type="file" name="file" <?php if(!$idUser)echo 'disabled';?>>
														<button class="btn buttons third" type="submit" <?php if(!$idUser)echo 'disabled';?>>Puja</button>
													</form>
													<!-- CATEGORIES & RETAILERS -->
													<div class="div_cat">
														<div class="div_cat1">
															<?php
															$a_categories = $myVideogame->llistaRel('categories', 'idVideogame= ' . $myVideogameTMP['idVideogame']);
															echo '<br>';
															echo '<h5>CATEGORIES:</h5>';
															foreach ($a_categories as $categories) {
																$idCategoria = $categories['idCategory'];
																$myCategory = new Category($conn);
																$a_cats = $myCategory->llista('idCategory=' . $idCategoria);
																foreach ($a_cats as $cats) {
																	echo '-	'.$cats['categoryName'];
																	echo '<br>';
																}
															}
															?>
														</div>
														<div class="div_cat2">
															<?php


															$a_retailers = $myVideogame->llistaRel('retailers', 'idVideogame= ' . $myVideogameTMP['idVideogame']);
															foreach ($a_retailers as $retailers) {
																$idRetailer = $retailers['idRetailer'];
																$myRetailer = new Retailer($conn);
																$a_cats = $myRetailer->llista('idRetailer=' . $idRetailer);
																foreach ($a_cats as $cats) {
																	echo '<h5>VENDEDOR:</h5>';
																	echo '-	'.$cats['retailerName'];
																}
															}
															?>
														</div>
													</div>
														</div>
													</div>
										<?php
										}
									}
								}
										?>
							</div>
				<?php
			}
		}
	}
				?>

						</article>
					</section>
					<section class="coments">
						<div class="container-fluid">
							<?php
							$userTMP = new userService($conn);
							$a_myComents = [];
							if ($a_myComents = $myComent->llista('idVideogame= ' . $idVideogameTMP, 'comentLikes')) {
								foreach ($a_myComents as $myComentTMP) {
									if ($a_users = $userTMP->llistar('iduser= ' . $myComentTMP['idUser'])) {
										foreach ($a_users as $userNames) {
											$idUserTMP = $userNames['iduser'];
											$userName = $userNames['username'];
										}
									};
									$a_votesComents = $myComent->llistaRel('idComent= ' . $myComentTMP['idComent']);
									foreach ($a_votesComents as $votes) {
										$idVotedUser = $votes['idUser'];
									}
									$a_votesComents = $myComent->llistaRel('idComent= ' . $myComentTMP['idComent']);
									$idVotedUserComent = [];
									foreach ($a_votesComents as $votes) {
										array_push($idVotedUserComent, $votes['idUser']);
									}
									$votedComent = false;
									if (in_array($idUser, $idVotedUserComent)) $votedComent = true;
							?>
									<div class="comentari col-12 p-2">
										<div class="col-md-9">
											<p>Comentari: <?= $myComentTMP['comentTxt'] ?></p>
											<p>Usuari: <?= $userName ?></p>
										</div>
										<div class="opcionsComentaris col-md-3 d-flex justify-content-around">
											<div class="d-block">
												<form class="form pb-3" action="private/Coments/comentsOperacions.php">
													<input type='hidden' name='idComent' value="<?= $myComentTMP['idComent'] ?>">
													<input type='hidden' name='idVideogame' value="<?= $idVideogameTMP ?>">
													<input type='hidden' name='public' value="public">
													<button class="btn buttons third" type='submit' name='operacio' value="Like" <?php if (!$idUser) {
																																		echo 'disabled';
																																	} else if ($votedComent) {
																																		echo 'disabled';
																																	} else {
																																		echo '';
																																	}; ?>><span class="iconify" data-icon="ion:thumbs-up" data-width="25"></span></button>
													<button class="btn buttons third ml-2" type='submit' name='operacio' value="Dislike" <?php if (!$idUser) {
																																				echo 'disabled';
																																			} else if ($votedComent) {
																																				echo 'disabled';
																																			} else {
																																				echo '';
																																			}; ?>><span class="iconify" data-icon="ion:thumbs-down" data-width="25"></span></button>
												</form>
												<div class="div_likes">
													<p>Likes: <?= $myComentTMP['comentLikes'] ?></p>
													<p class="pl-2">Dislikes: <?= $myComentTMP['comentDislikes'] ?></p>
												</div>
											</div>
											<form action='private/Coments/comentsOperacions.php'>
												<input type='hidden' name='idComent' value="<?= $myComentTMP['idComent'] ?>">
												<input type="hidden" name="idVideogame" value="<?= $idVideogameTMP ?>">
												<input type='hidden' name='public' value="public">
												<input type='hidden' name='operacio' value="elimina">
												<button class='btn buttons third p-1' type='submit' <?php if (!$idUser || $idUser != $idUserTMP) echo 'disabled'; ?>><span class="iconify" data-icon="ion:trash" data-width="25"></span></button>
											</form>
										</div>
									</div>
							<?php
								}
							}
							?>
							<div class="comentTxt">
								<form action="private/Coments/comentsOperacions.php">
									<input type="hidden" name="operacio" value="insertar">
									<input type="hidden" name="idVideogame" value="<?= $idVideogameTMP ?>">
									<textarea name="comentTxt" id="comentTxt" rows="5" style="width: 100%;" <?php if (!$idUser) echo 'disabled'; ?>></textarea>
									<div id="div_comentTxt" style="width: 20%;">
										<button class='btn buttons third' type="submit" <?php if (!$idUser) echo 'disabled'; ?>>Comenta</button>
									</div>
								</form>
							</div>
						</div>
					</section>
				</div>
				<?php
				require './footer.html';
				require './scripts.html';
				?>
			</body>

</html>