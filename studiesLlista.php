<?php 
//0) activo els errors
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

//1) Activo la sessió
session_start();


if(isset($_SERVER['CONTEXT_DOCUMENT_ROOT'])){
    $path =$_SERVER['CONTEXT_DOCUMENT_ROOT'];
}
else{
    $path = $_SERVER['DOCUMENT_ROOT'];
}

include_once($path.'/conf/conf.php');
require './header.php';

$myStudy = new Study($conn);
if(!isset($_REQUEST['filtre'])) $filtre = '';
else $filtre = $_REQUEST['filtre'];
$sql = 'studyName LIKE  \'%' . $filtre . '%\'';
$total = $myStudy->numTotal($sql);
$results_per_page = 10;
$numPag = ceil($total/$results_per_page);
if (!isset ($_GET['page']) ) {  
	$pageActual = 1;  
} else {  
	$pageActual = $_GET['page'];  
}
$page_first_result = ($pageActual-1) * $results_per_page;
$limit = $page_first_result.','.$results_per_page;
?>
<head>
	<link rel="stylesheet" href="/studiesLlista.css">
</head>

<body>
	<h1 class="text-center mt-2">ESTUDIS</h1>
	<div class="container-fluid llista">
		<section>
		<form action="" class="search" autocomplete="off">
			<input type="text" placeholder="Buscar per nom..." name="filtre" onblur="submit()" value="<?=$filtre?>">
		<table class="table table-hover table-striped">
			<tbody>
		<?php
		$a_myStudy = [];
		if($a_myStudy = $myStudy->llista($sql,$limit)){
			foreach($a_myStudy as $myStudyTMP){
			?>
				<tr>
					<td class="col-2 foto"><a href="/study_details.php?value=<?=$myStudyTMP['idStudy']?>"><img class="list-img" src="<?=$myStudyTMP['studyLogo']?>" alt=""></a></td>
					<td class="col-4 col-md-6"><a class="h5 text-white" href="/study_details.php?value=<?=$myStudyTMP['idStudy']?>"><?=$myStudyTMP['studyName']?></a><p class="pText"><?=$myStudyTMP['studyBio']?></p></td>
					<td class="col-2"><p class="pLlista">Likes: <?=$myStudyTMP['studyLikes']?></p><p class="pLlista mb-0">Dislikes: <?=$myStudyTMP['studyDislikes']?></p><br>
					<p class="pStar">
					<?php 
					$cont = 5;
					for($i =0;$i<$myStudyTMP['studyRating'];$i++){
						echo "★";
						$cont--;
					}
					for($i=0;$i<$cont;$i++){
						echo "☆";
					} 
					?>
					</p></td>
				</tr>
			<?php 
			}
		}
		?>
			</tbody>
		</table>
		<nav class="paginacio">
			<ul class="pagination justify-content-center">
				<li class="page-item <?php if($pageActual==1)echo 'disabled';?>">
					<a class="page-link" href="<?=$_SERVER['PHP_SELF']. '?page=' . ($pageActual-1).'&filtre='.$filtre ?>">Previous</a>
				</li>
				<?php
				for($page = 1; $page<= $numPag; $page++) {  
				?>
				<li class="page-item nums <?php if($pageActual == $page)echo"active";?>"><a onclick="submit()" class="page-link" href = " <?=$_SERVER['PHP_SELF']. '?page=' . $page.'&filtre='.$filtre ?> "> <?=$page?> </a></li>
				<?php
				}
				?>
				<li class="page-item <?php if($numPag<=$pageActual)echo 'disabled'; ?>">
					<a class="page-link" href="<?=$_SERVER['PHP_SELF']. '?page=' . ($pageActual+1).'&filtre='.$filtre ?>">Next</a>
				</li>
			</ul>
			<p class="text-center paginaActual">Pagina: <?=$pageActual?> de <?=$numPag?></p>
		</nav>
		</form>
	</section>
	</div>
    <?php
	require './footer.html';
	require './scripts.html';
	?>
</body>

</html>